<div align="center">
<h1>abcd : <b>A</b>lpine <b>B</b>ased <b>C</b>os <b>D</b>erivative</h1>

 ![abcd logo](pictures/abcd-logo-280x165.png "abcd"){: .shadow}
 </div>

<div align="justify">

**Keywords :**  Kairos derivative; polymorphic OS images; immutablity; installless OS image; execution context agnostic; "rootless" Containerized OS (sandboxing).

I share here some personal notes about Kairos derivatives (sorry maybe a bit long), hoping it can be useful for others or maybe receive some feedback or guidelines from the Kairos community.

_
> **Disclaimers :** This is a work in progress, not yet fully completed, some issues still need to be investigated (see at end of page...).
_

## polymorphic OCI OS image with Kairos

### Context

Orphan of K3OS as a base system for CIRRUS (Cloud Iac Reservoir de Resources pour Sessions de tp) a pedagogical lab cluster. I successively turned to C3OS, then elemental-toolkit and finally naturally [Kairos](https://github.com/kairos-io/kairos) to build base system ISO images for lab environments. For the fourth version of [VIMINAL (**VI**rtual **M**odel for **I**p **N**etwork **A**rchitecture **L**ab)](https://sourceforge.net/projects/viminal/files/),  initially used as autonomous installless all in one LiveDVD network lab plateform, now essentially played as live ISO on HVM Hypervisor, I plan to switch from Gentoo catalyst live iso generator to the lighter Kairos/Aurora iso builder. I'll switch from Gentoo to Alpine distribution, which was the one used on K3OS, for its neat lightness and as a more than 20 years Gentoo user I'm more familar with its openrc init process. Prefix name of these images is ABCD which means **A**lpine **B**ased **C**os **D**erivative (_Cos stands for "Containerized Opernating System"_). But all that's describe in this post can, with no doubt, be translated to more common systemd like Linux distributions as [Kairos is "Meta-Distribution, distro agnostic"](https://kairos.io/docs/).

**_Essential properties for that goal :_**

- immutability : induced on initial liveDVD context that's a key property of Kairos environnment for ephemeral renewable lab with almost no surprises at lab session startup, as well for node startup for k3s CIRRUS cluster. Persistency, not yet managed on VIMINAL, will be based on COS_PESISTENCE auxilliary block strorage capability of Kairos. 
- installless system : although A/B automatic install mode of Kairos is the right way to manage immutable always up-to-date base system. I'd like to stay in installless mode. 
  - For VIMINAL, lab sessions are ephemeral, on common computer of lab rooms so : 
    - no config changes on host ("You are requested to return the machine in the state in which you found it when booting") 
    - and usually  no admin rights on the common computer lab hosts.
  - For CIRRUS cluster, on core control nodes, I'd like to dedicate all disks for the distributed ROOK/Ceph storage backend, so system drive will be on auxilliary USB key drive not on disk and config or system persistence managed on COS_PERSITENT labeled auxilliary usb drive. Ephemeral compute nodes, will be on diskless PXE or even better IKU boot.
_

> **Disclaimers :** abcd derivatives are full system docker images. If you're looking for tiny sized microservice docker images, you're not at the right place. System images can be huge, some abcd derivatives especlially full desktop (abcd4desktop descendants) can be up to several GiB.
_

> **Disclaimers :** abcd4desktop derivatives for VIMINAL are full desktop systems (SDDM/KDE/Plasma). I'm aware that such systems are not [Kairos](https://kairos.io/) system target. But Polymorphic OCI image property described on this post could also be useful as well for the infrastructure system OS images.
_ 

### Polymorphic OS image running on all major common execution contexts

In that context of networked lab execution, the pure performance of lab system nodes is less important than the flexibility of execution environments that typically run nested. The nodes on which the students carry out manipulations are generally virtualized in the form of an appliance (all in one VM), as the first virtualization level, and the entire laboratory platform (VIMINAL, but also the dominant GNS3 system image in the context of teaching network infrastructure) is itself executed in virtualized mode (second virtualization level aka nested). At MOOCs (Massive Open On line Courses) scale, that laboratory platform must be able to be run for the entire subscriber population. It needs to be distributed as a simple VM image with the minimum of runtime dependencies, to be run on common HVM hypervisors such Virtualbox (available on the 3 dominant systems, i.e. GNU/Linux, Windows and IOS), VMWare or Hyper-V, as well as libvirt/KVM (but for the latter only for Linux subscribers).

As OSaC (Operating System as Code) environment, basically based on Dockerfile format, Kairos use standard OCI system image as its immutable rootfs packaging for baremetal or HVM-VM install. Several years ago, VIMINAL network lab model environment, as its third version, switched from nested HVM (libvirt/KVM) execution context to an [LXD (lightvisor)](https://ubuntu.com/lxd) execution context. So idea arose, why not try running Kairos system image on the common LXD container runtime as well as dominant Docker or Podman. In that way the same single base system image could be run on, not all but, almost all major common execution contexts : baremetal (liveUSB ISO), HVM hypervisor (KVM/libivrt, VirtualBox, VMWare, Hyper-V, XVisor,... in liveISO mode) and container runtime (LXD, Docker, Podman, ...) paving the way for VDI (Virtual Desktop Infrastrucure) scaling up to today's dominant cloud envrionment i.e. K8S. 
_
> **Disclaimers :** I know that  containerized os images break the primitive dogma enacted by Docker ("one process per container you'll execute"), that's why Canonical designed [LXD as "lightvisor"](https://ubuntu.com/lxd) to containerize full systems. But on k8s clusters LXD runtime is not common.
_ 
### inheritance and abcd lineage

OCI container images stack several storage layers at build time, one layer per step of the dockerfile. Multiple stacking levels can be aggregated when importing a previously built image. So one system image can share some common genes of ancestor system. The system image can thus be managed by inheritance, as in the OOP programming language. The polymorphic image adds an upper layer to a common functional image, whose elements are adapted to the target execution context. The aim is to ensure that, whatever the target execution context, the same functionalities, user interface and user experience are preserved.

So, for a basic ABCD image, these directives for the upper layer are set inside dedicated dockerfiles :

- abcd**ic** : abcd **I**nside **C**ontainer to build container runtime conformant system image ;

- abcd**ik** : _abcd with **I**nitrd and **K**ernel : add initrd and kernel elements which will be used by Aurora to build iso full system image bootable on bare metal or on full virtalization hypervisors (HVM)._ **Deprecated** Since kairos 2.4.3 native flavor kernel and initrd building migrate from earthly workflow to inside the root ancestor (aka abcd) dockerfile. So no more need of dedicated descendant abcdik image to build live iso.

- abcd**ib** : (just a crazy idea not yet implemented) abcd **I**nside **B**rowser : Another lab platform called [L.I.B.E.R.A.L. (**L**inux **I**nside **B**rowser to **E**xperiment **R**emotely **A**ctivities and **L**abs)](https://gitlab.com/j-landru/liberal) is an in browser remote lab use case based on Fabrice Bellard [JSLinux javascript CPU emulator](https://bellard.org/jslinux/). LIBERAL was conceived at IMT Nord Europe during that unusual COVID19 pandemic period where containment forced to play some  labs remotely at student's home. Based on a javascript hardware emulator like jor1k or JSLinux, this kind of platform aimed at playing basic Linux system labs directly inside the browser with no system depeedencies nor hypervisor install. On such Javascript emulator performance of the virtualized system is modest, but sufficient for shell cli manipulations. JSLinux emulates X86 or RISC-V architecture, so abcdib dockerfile would be the recipe for building LIBERAL lab images, but need to investigate Kairos on RISC-V, which would undoubtedly be another exciting story...

### abcd family tree

```

                                                    +------+
                                                    | abcd | _kairos Alpine base_ 
                                                    +------+
                                                       |
                    +------------------------------------------------------------------------+
                    |                                                                        |
            +----------------+                                                      +----------------+
            | abcd4cirrus-os |  _kairos Alpine_                                     |  abcd4desktop  |   _Alpine desktop_
            +----------------+ _k3s base system_                                    +----------------+   _sddm/kde/plasma_
                    |                                                                        |
           +-----------------+                     +---------------+------------------+--------------------------------------+
           |                 !                     |               !                  |                                      |
 +----------------+        (iso)           +--------------+      (iso)         +------------+                       +------------------+
 |abcdic4cirrus-os|                        |abcdic4desktop|                    |abcd4viminal| _VIMINAL base_        |abcd4objectif-ipv6| _objectif IPv6 MOOC_
 +----------------+                        +--------------+                    +------------+                       +------------------+ _GNS3 lab platform_
         !                                        !                                  |                                       |
    (container)                              (container)                    +-----------------+                  +-----------------------+
                                                                            |                 !                  |                       !
                                                                     +--------------+       (iso)      +--------------------+          (iso)
                                                                     |abcdic4viminal|                  |abcdic4objectif-ipv6|
                                                                     +--------------+                  +--------------------+
                                                                            !                                    !
                                                                       (container)                          (container)

- abcd : ancestor of the line (kairos Alpine reference dockerfile);
- abcd4cirrus-os : k3s system for CIRRUS cluster nodes;
- abcd4desktop : Alpine kde/plasma desktop base image;
- abcd4viminal : base image for VIMINAL 3.x lab collection (katara network labs);
- abcd4objectifipv6 : Objectif IPv6 MOOC labs (GNS3 IPv6 network models).
```

**_Nota :_** On abcd4desktop line subtree, graphical display is managed by classical X11 X server on physical or paravirtualized (such spice/qxl) graphic card drivers for abcdik descendants and xpra/Xdummy for abcdic descendants. On that latter, remote desktop is displayed in xpra REWAARD mode (**_R_**emote **_E_**nvironment via **_W_**eb **_A_**ccess with **_A_**uto **_A_**djsuted **_R_**esolution **_D_**isplay) : xpra HTML5 web rest interface with dynamic resolution auto-adjustment to browser window size. See illustration below and [SDDM/KDE/PLASMA remote desktop with xpra](https://github.com/orgs/Xpra-org/discussions/3911) for  config details.

### About rootless container

In this context of running academic labs, without privileged rights on the lab workstation (sandbox), it is necessary to run the containers in unprivileged mode (and more generally, it is a good practice to run with less privileges). So the  environment settings to run the container abcd in rootless mode is the good way, _(Another argument was to get around a window manager startup issue inside container see : "[unbearable delay at sddm login/logout starting kde Plasma desktop when running inside container](https://framagit.org/j-landru/abcd/-/issues/2)" for more details)_.


### TODO  - What else next ?!

- cgroupv2 rootless pod to run virtual desktop on k3s ;
- native Alpine kernel and initrd in replacement of kairos-alpine-ubuntu to build iso - [pure, Alpine flavor, which is now independent from systemd, stayed tuned](https://kairos.io/blog/2023/06/15/kairos-release-v2.2/) ;
- cloud-init in the container runtime context to align container startup seamlessly with the equivalent baremetal/VM dynamic configuration ?
- for small image [booting live iso to ram](https://github.com/rancher/elemental-toolkit/issues/1558), but UKI will be even better !! [Introducing Kairos 2.0: long live UKI!](https://kairos.io/blog/2023/04/13/kairos-release-v2.0/) useful for cirrus diskless compute node with WOL wake-up ?
- help or guidelines from Kairos community for COS_PERSISTENT on f2fs block device (see [ COS_PERSISTENT on a F2FS partition howto ? #1470 ](https://github.com/rancher/elemental-toolkit/issues/1470) ;
- abcd4desktop new descendant :  aawwoo  **A**lternative **A**ppliance **W**hen **W**indows is **O**ut of **O**rder. A  SDDM/KDE/Plasma desktop disguised windows11 to help in case of emergency such disaster recovery when under crypto locking attack. Start crypto locked host in live mode without its damaged disk to furnish partial "corprorate" workload capabilities (web browsing - web mail - Libreoffice - ...) or VDI appliance started on a k8s  rescue cluster ?!
- Virtual Desktop Infrastructure : on demand remote desktop as a service (ODRDaaS) at scale on k8s cloud could be considered on several ways : 
    - on k8s cluster run abcdi*c*4desktop descendants on demand as one pod per user (personnal persistant data mounted at startup or login under /home eventually through keyed sshfs ?)
    - on k8s cluster run abcdi*k*4desktop descendants on demand as one kubevirt pod per user (personnal persistant data mounted at startup or login under /home eventually keyed sshf ?) that latter heavier than former but more confined
- investigate SSLH to study ingress flow multiplexing on single secure 443 port, (goal : end2end encrypting flows, reducing filetring rules, single cert key per pod, ???) in replacement of actual ssh tunnel multiplexing ??
- ...

### Illustration

**Does it work ? 1st try** 

On a Gentoo host, 4 simultaneous abcd4desktop instances on 4 execution contexts : from left to right and top down
 abcd**ik**4desktop liveiso HVM (kvm/libvirt spice/qxl paravirtualized graphic) ; abcd**ic**4desktop rootless docker container (html5 remote display on firefox) ; abcd**ic**4desktop rootless podman container (html5 remote display on firefox) ; abcd**ic**4desktop lxd rootless container (html5 remote display on firefox).

 ![4 simultaneous instances (SDDM greeter)](pictures/abcd4desktop-hvm-libvirt-kvm-and-rootless-containers-docker-podman-lxd-sddm-greeter-20230926-3440x1440.png)

automatic resolution adjustment to browser tab size on KDE/Plasma destop

![4 simultaneous instances (Plasma desktop)](pictures/abcd4desktop-hvm-libvirt-kvm-and-rootless-containers-docker-podman-lxd-kde-plasma-desktop-20230926-3440x1440.png)

</div>

## HOWTO...
<details><summary>**HOWTO** (Click to expand)</summary>

### Baremetal or fully virtualized VM execution context

**abcdik dockerfile :** abcdik is the "pre-aurora" layer which add elements sufficient to buiid iso with aurora. At time of writing, for Alpine derivative, it's based on Kairos ```master-alpine-ubuntu```. On near future; [pure, Alpine flavor, which is now independent from systemd, stay tuned](https://kairos.io/blog/2023/06/15/kairos-release-v2.2/) hope it can be native Alpine based...

`abcdik4destkop` dockerfile

```
######### caution this is abcdiKKKKKKKKKKKKKKKKKKKKKKK Dockefile not to be confused with abcdic one !!!

ARG BASE_IMAGE=<your-registry>/abcd4desktop:latest
FROM $BASE_IMAGE


# Copy the Kairos framework files. We use master builds here for fedora. See https://quay.io/repository/kairos/framework?tab=tags for a list
#COPY --from=quay.io/kairos/framework:master_fedora / /
# copy kernel and initrd from alpine ubuntu waiting alpine lts and alpine initrd guidleines...
COPY --from=quay.io/kairos/framework:master_alpine-ubuntu / /
#COPY kairos-alpine-ubuntu* /boot/

```

build image, and push it on your registry
```
docker build --no-cache -t <your-registry>/abcdik4desktop -t abcdik4desktop .
docker images
docker push <your-registry>/abcdik4desktop:latest
```
build iso using aurora tool (as describe [Build Kairos from scratch](https://kairos.io/docs/reference/build-from-scratch/) or [AuroraBoot](https://kairos.io/docs/reference/auroraboot/)

**_Nota_** Building iso with Aurora has to run in privileged container (else error

`"ERR Failed pulling container image 'jlandru/abcdik4desktop:latest' to '/tmp/auroraboot/temp-rootfs' (local: true): exit status 1"`

so when running dockerd in rootless mode with userns-remap to isolate containers with a user namespace (recommanded) you have ot add `--privileged --userns=host` when executing aurora build container 

```
docker run --privileged --userns=host -v "$PWD"/build:/tmp/auroraboot -v /var/run/docker.sock:/var/run/docker.sock \
		--rm -ti quay.io/kairos/auroraboot:latest \
		--set container_image=docker://<your-registry>/abcdik4desktop:latest \
		--set "disable_http_server=true" --set "disable_netboot=true"  \
		--set "state_dir=/tmp/auroraboot"
```

rename kairos.iso with the name of your choice (as Aurora does not have yet time-dated suffixed rename facility as elemental-toolkit had)

```
mv build/iso/kairos.iso build/iso/abcd4desktop-$(date +%Y%m%d).iso
mv build/iso/kairos.iso.sha256 build/iso/abcd4desktop-$(date +%Y%m%d).iso.sha256 && \
       sed -i "s/kairos.iso/abcd4desktop-$(date +%Y%m%d).iso/" build/iso/abcd4desktop-$(date +%Y%m%d).iso.sha256
```
- Running : 

you can now block copy (using ```dd``` command) iso file to an usb-key to live boot baremetal host or live boot a VM with iso file as laoded CDROM with your usual hypervisor

In the grub menu chose ```Kairos (boot local node from livecd)``` entry.

**To do :** Generate iso with a dedicated personalized ```grub.cfg``` menu containing single default choice to live boot abcd !!!


### Container execution contexts

#### Common OCI guidelines

Launching full system inside a docker container needs some context adjustements before starting ```init``` process as PID 1. Found some detailed guidelines on ["Building, Using, and Monitoring WireGuard Containers"](https://www.procustodibus.com/blog/2021/11/wireguard-containers/) and ["Docker Alpine openrc"](https://github.com/robertdebock/docker-alpine-openrc).

`abcdic4destkop` dockerfile

```
######### caution this is abcdiCCCCCCCCCCCCCCCCCCCC Dockefile not to be confused with abcdik one !!!

ARG BASE_IMAGE=<your-registry>/abcd4desktop:latest

FROM $BASE_IMAGE

# abcd : Alpine Base Cos Derivative is an Alpine unofficial flavor of Kairos (former elemental-toolkit) derivative
# abcd family will be devivated in two branchs 
#  - abcd4cirrus-os : a kairos/k3s image for cirrus cluster node : an epehmaral installess imutable os (live iso or pxe) for cirrus cluster node
#  - abcd4desktop   : a kde/plasma desktop image
#                     for
#                         next generation of viminal (Virtual Model for Ip Network Architecture Labs)
#                         see https://sourceforge.net/projects/viminal/ 
#                     and
#                         aawwwoo (Alternative Appliance Working When Windows is Out of Ordrer) 
#                         a  kde plasma desktop disguised as win11 to live boot in diskless mode a PC,
#                         in case of emergency of crypto highjacked/locked MSWin workstation)
#
#  abcd polymorph OCI : an attempt to use the same system OCI based image in serveral vitualization execution contexts
#
#    abcdic : abcd Inside Container : add all the stuff needed to start the abcd base system image inside a container
#             (lxd, lxc, docker, podman, k8s,...)
#
#    abcdik : abcd with Initrd and Kernel : abcd OCI system image used by Kairos AuroraBoot tool to build iso image 
#             runnable on bare metal or under common hyperivsiors (kvm, virtualbox, VMware, Hyper-V, Xen, kubevirt, openstack,...)
#             in live mode (live USB for bare metal or live ISO for HVM contexts)
#
# abcdic dokcer file : add all the stuff needed to start the base system image inside a container
#
# Alpine is an openRC init distribution, so need this init start inside container,
#        (probably needs adjustment for systemd distribution family)
# this Docker file is inspired from : https://www.procustodibus.com/blog/2021/11/wireguard-containers/
# without wireguard stuff

# already present in base abcd OCI
# RUN apk add --no-cache \
#     openrc

## 20230310 (inspired from https://www.procustodibus.com/blog/2021/11/wireguard-containers/)
##RUN \
##    # 1) sed -i 's/^\(tty\d\:\:\)/#\1/' /etc/inittab: Prevents unnecessary tty instances from starting.
##    sed -i 's/^\(tty\d\:\:\)/#\1/' /etc/inittab && \
##    # 2) sed -i 's/^#\?rc_env_allow=.*/rc_env_allow="\*"/' /etc/rc.conf: Propagates Docker environment variables to each OpenRC service.
##    # 3) sed -i 's/^#\?rc_sys=.*/rc_sys="docker"/' /etc/rc.conf: Lets OpenRC know it’s running in a Docker container.
##    sed -i \
##        -e 's/^#\?rc_env_allow=.*/rc_env_allow="\*"/' \
##        -e 's/^#\?rc_sys=.*/rc_sys="docker"/' \
##        /etc/rc.conf && \
##    # 4) sed -i 's/VSERVER/DOCKER/' /lib/rc/sh/init.sh: Makes sure the /run directory is set up appropriately for a Docker container.
##    # 5) sed -i 's/checkpath -d "$RC_SVCDIR"/mkdir "$RC_SVCDIR"/' /lib/rc/sh/init.sh: Ensures the needed /run/openrc directory exists.
##    sed -i \
##        -e 's/VSERVER/DOCKER/' \
##        -e 's/checkpath -d "$RC_SVCDIR"/mkdir "$RC_SVCDIR"/' \
##        /lib/rc/sh/init.sh && \
##    # 6) rm /etc/init.d/hwdrivers: Prevents an ignorable error message from the unneeded hwdrivers service.
##    # 7) rm /etc/init.d/machine-id: Prevents an ignorable error message from the unneeded machine-id service.
##    rm \
##        /etc/init.d/hwdrivers \
##        /etc/init.d/machine-id

## 20230613  init services so long at startup and high CPU consomption ??
##          so trying to replace above section with sed substitution found at https://github.com/robertdebock/docker-alpine-openrc
RUN  \
    sed -i 's/^\(tty\d\:\:\)/#\1/g' /etc/inittab && \
    sed -i \
      -e 's/#rc_sys=".*"/rc_sys="docker"/g' \
      -e 's/#rc_env_allow=".*"/rc_env_allow="\*"/g' \
      -e 's/#rc_crashed_stop=.*/rc_crashed_stop=NO/g' \
      -e 's/#rc_crashed_start=.*/rc_crashed_start=YES/g' \
      -e 's/#rc_provide=".*"/rc_provide="loopback net"/g' \
      /etc/rc.conf && \
    rm -f /etc/init.d/hwdrivers \
      /etc/init.d/hwclock \
      /etc/init.d/hwdrivers \
      /etc/init.d/modules \
      /etc/init.d/modules-load \
      /etc/init.d/modloop && \
    sed -i 's/cgroup_add_service /# cgroup_add_service /g' /lib/rc/sh/openrc-run.sh && \
    sed -i 's/VSERVER/DOCKER/Ig' /lib/rc/sh/init.sh
##

## 20230615  to see more for slow start investigation...
RUN \
  echo "rc_verbose=yes" >> /etc/conf.d/syslog && \
  echo "rc_verbose=yes" >> /etc/conf.d/dbus

# copy specific abcdic files
COPY overlay/files-abcdic/ /


# 20230629 remote desktop through html5 with resolution automatically adjusted on browser window size
#          "xpra upgrade desktop" launched automatically at each sddm session when sddm reload xorg
#          add xpra script inside /usr/share/sddm/scripts/Xsetup

RUN cat <<EOF >> /usr/share/sddm/scripts/Xsetup
#
# launch xpra upgrade-desktop for html5 remote desktop
/etc/xpra/scripts/xpra-remote-desktop.sh
#
EOF


# 20230808 trying workaround elogind issue by migrating to cgroupv2 only rc_cgroup_mode="unified" not "hybrid" on openrc
#  from gentoo openrc reference : https://wiki.gentoo.org/wiki/OpenRC/CGroups
# see https://framagit.org/j-landru/abcd/-/issues/2
RUN cp /etc/rc.conf /etc/rc.conf.orig.bis && \
    sed -i \
      -e '/#rc_controller_cgroups="YES"/c\#rc_controller_cgroups="YES"\nrc_controller_cgroups="YES"' \
      -e '/#rc_cgroup_mode="hybrid"/c\#rc_cgroup_mode="hybrid"\nrc_cgroup_mode="unified"' \
      -e '/#rc_cgroup_controllers=""/c\#rc_cgroup_controllers=""\nrc_cgroup_controllers="cpuset cpu io memory hugetlb pids"' \
    /etc/rc.conf

    
# CMD ["/sbin/init"]: Boots OpenRC on container start.
##CMD ["/sbin/init"]
###CMD /usr/bin/cgroupfs-mount && exec /sbin/init
CMD [ "/sbin/init" ]
```
- build image, and push it on your registry
```
docker build --no-cache -t <your-registry>/abcdic4desktop -t abcdic4desktop .
docker images
docker push <your-registry>/abcdic4desktop:latest
```

#### Running abcdic rootless container with Docker, podman, LXD

Each container execution context has its own local image repository uncompliant each others. So docker OCI image has to be pulled and eventually adjusted to be imported in that container local repository.

Common rootless config for Docker, podman, lxd : 
On host set `/etc/subuid` **and** `/etc/subgid` shift for user running the container

Example from my host : `/etc/subuid` and `/etc/subgid` file content
```
root:1000000:1000000000
<your-id>:1001000000:1000000
lxd:1002000000:1000000
lxc:1003000000:1000000
```
some other namespace config adjustements may be needed for each container runtime, see bellow.

##### Docker :

**_Nota :_** `--ulimit nofile=1024:1024` justidication see https://framagit.org/j-landru/abcd/-/issues/1


Docker environment settings for abcdic4desktop rootless containers 

- Set IPv6 (not an option in 2023 and needed for my xpra html5 remote desktop config, see [Howto configure SDDM/KDE/PLASMA remote deskop with xpra](https://github.com/orgs/Xpra-org/discussions/3911)
-   rootless container (as stated above)
            - Set cgroupv2 ans userns-remap for rootless container execution (/etc/subuid /etc/subgid ans daemon.json )
            - add --cgroupns-private to docker run
            - [see also](https://framagit.org/j-landru/abcd/-/issues/2)


for Docker (accordingly to Docker guideline : [rootless mode](https://docs.docker.com/engine/security/rootless/) and [container isolation with user namespace](https://docs.docker.com/engine/security/userns-remap/) ) adjust `/etc/docker/daemon.json` conf file to set `userns-remap` to &lt;your-id&gt;

Example from my host : `/etc/docker/daemon.json`

```
{
  "userns-remap": "<your-id>",
  "ipv6": true,
  "experimental": true,
  "fixed-cidr-v6": "2001:db8:1956:d0c4::/64",
  "ip6tables": true,
  "exec-opts": ["native.cgroupdriver=systemd"],
  "features": { "buildkit": true },
  "experimental": true,
  "cgroup-parent": "docker.slice"
}
```
**** Nota :**** `fixed-cidr-v6` illustrated with `2001:db8::/32` IPv6 documentation prefix, adjust to your GUA or ULA /64 prefix
Pull the latest abcdic4desktop image to your local repository and then launch docker container.

```
docker pull <your-registry>/abcdic4desktop:latest
docker images
docker run --cgroupns=private --network=bridge --ulimit nofile=1024:1024 --name abcdic4desktop --rm -ti <your-registry>/abcdic4desktop:latest
dokcer ps
```
to test remote desktop using ssh tunnel from Docker host : 
- open a container shell to retrieve local adress of the container (example on IPv6 LLA adress of the container, adjust to alternate IPv6 or IPv4 address if any)

```
docker exec -ti abcdic4desktop /bin/bash
ip -6 addr show eth0
```
- open ssh tunnel targeting  xpra-html5 port 8080 inside the container (user:  abcd / passwd: abcd) through default docker0 bridge interface on host. _(note adjust to your container IPv6 LLA address)_
 
 ```
 ssh -L 8080:[::1]:8080 abcd@fe80::42:acff:fe11:2%docker0
 
 ```
 - On host point your html5 browser to port 8080 on localhost ```http:[::1]:8080/``` to access auto adjusting resolution remote desktop of the container. May need to reload page to display desktop. See [SDDM/KDE/PLASMA remote deskop with xpra](https://github.com/orgs/Xpra-org/discussions/3911) for more details. 

##### Podman :

- Set IPv6 (not an option in 2023 and needed for xpra html5 remote desktop config, see [Howto configure SDDM/KDE/PLASMA remote deskop with xpra](https://github.com/orgs/Xpra-org/discussions/3911)
IPv6 on rootless podman container need podman 4.0, or above, with netavark network tool see [https://github.com/containers/podman/blob/main/docs/tutorials/basic_networking.md#bridge](https://github.com/containers/podman/blob/main/docs/tutorials/basic_networking.md#bridge)

Default rootless bridge with netavark remain IPv4 only, so Set another bridge IPv6 compliant

```
$ podman network create --ipv6 abcdnet
```

Pull abcdic OCI image on local podamn repository

```
podman pull <your-repository>/abcdic4desktop
podman images
```

Launch podman container with the almost same syntax as Docker but attached to abcdnet IPv6 bridge (as default podman bridge is not IPv6 friendly by default) 
needs some more investigation about rootless/podman/netavark - unable to communicate on abcdnet even when joining the extra network namespace using `podman unshare --rootless-netns` see [https://github.com/containers/podman/blob/main/docs/tutorials/basic_networking.md#bridge](https://github.com/containers/podman/blob/main/docs/tutorials/basic_networking.md#bridge). So, workaround export ssh container port to 2222 host port to multiplex flow inside local ssh tunne. 

```
podman run   --ulimit nofile=1024:1024 --name abcdic4desktop --rm -ti --network abcdnet -p 2222:22 <your-registry>/abcdic4desktop:latest
```

to local test remote desktop through ssh tunnel from podman host : 
 
 ```
 ssh -p 2222  -L 8086:[::1]:8080 abcd@localhost
 
 ```
 - On host point your html5 browser to port 8086 on localhost ```http:[::1]:8086/``` to access auto adjusting resolution remote desktop of the container. May need to reload page to display desktop. See [SDDM/KDE/PLASMA remote deskop with xpra](https://github.com/orgs/Xpra-org/discussions/3911) for more details. 
 
 ##### LXD :
 
 That container runtime manage its own image format, so abcdic OCI image has to be translated during import on LXD local image store. LXD (lxc command) import need two archives :  a minimal metadata.yaml describing the imported image and archive of the system rootfs.
 
 - generate up to date minimal metadata.yaml file and its tar archive
 
 ```
echo -e "architecture: x86_64\\ncreation_date: "$(date +%s)"\\nproperties:\\n  description: abcdic4desktop (Alpine edge) "$(date +%Y%m%d)"\\n  os: Alpine\\n  release:\\n    -edge\\n    -"$(date +%Y%m%d) > metadata.yaml
# verify
cat metadata.yaml
# generate tar archive
tar -cvf abcdic4desktop-lxd-metadata.yaml.tar metadata.yaml
```

- generate a temporary rootfs archive xz compressed archive image from the OCI image :
***** Nota ! **** This step has to be done in rootful mode (need some investigation to switch buildah mount in rootless mode)
    a) run OCI image with podman (need latest OCI image loaded on podman local store (see above);
    b) buildah mount that container;
    c) create an ephemeral compressed archive (tar.xz) of that mounted container rootfs;
    d) rm the ephemeral podman container

```
#  - run OCI image with podman
podman run --network=bridge --ulimit nofile=1024:1024 --name abcdic4desktop --rm --detach <your-registry>/abcdic4desktop
# - buildah mount that container 
# - create an ephemeral compressed archive (tar.xz) of that mounted container rootfs
tar --directory=$(buildah mount abcdic4desktop) -cvJf /tmp/abcdic4desktop.tar.xz .
# - rm the podman container
podman rm --force abcdic4desktop
```

- import the two archive files in local LXD image repository

```
lxc image import abcdic4desktop-lxd-metadata.yaml.tar /tmp/abcdic4desktop.tar.xz  -v --alias abcdic4desktop
# verify
lxc image list
# keep previous old one to eventually rollback, but free lxd storage for oldest than previous
# lxc image delete xxxxxfingerprintxxxx
```
- Give it a try

```
# To test/run image under lxd (-e flag for ephemral)
lxc launch abcdic4desktop abcdic4desktop -e
# verify running
lxc list
```
to test remote desktop using ssh tunnel from lxd host : 
- open a container shell to retrieve local adress of the container (example on IPv6 LLA adress of the container, enventually adjust to alternate IPv6 or IPv4 address if any)

```
# local TTY access
lxc exec abcdic4desktop /bin/bash
ip -6 addr show eth0
```
- open ssh tunnel targeting  xpra-html5 port 8080 inside the container (user:  abcd / passwd: abcd) through default lxdbr0 bridge interface on host. _(note adjust to your container IPv6 LLA address)_ 
 
 ```
 ssh -L 8088:[::1]:8080 abcd@fe80::216:3eff:fe52:aa6b%lxdbr0
 ```
- On host point your html5 browser to port 8088 on localhost ```http:[::1]:8088/``` to access auto adjusting resolution remote desktop of the container. May need to reload page to display desktop. See [SDDM/KDE/PLASMA remote deskop with xpra](https://github.com/orgs/Xpra-org/discussions/3911) for more details. 
 


*****Nota : LXD issue :***** Needs more investigation :  Strangely generating the rootfs archive from docker/podman save command (in rooutful as well as rootless mode)
```
docker image save jlandru/abcdic4desktop:latest | xz > /tmp//tmp/abcdic4desktop.tar.xz

lxc image import abcdic4desktop-lxd-metadata.yaml.tar /tmp/abcdic4desktop.tar.xz  -v --alias abcdic4desktop
```

results in an unusable lxd image. Container failed to start (rootful as well as rootless). Displaying : 
```
$ lxc launch abcdic4desktop abcdic4desktop -e
Création de abcdic4desktop

Démarrage de abcdic4desktop                           
Error: Failed to run: /usr/bin/lxd forkstart abcdic4desktop /var/lib/lxd/containers /var/log/lxd/abcdic4desktop/lxc.conf: exit status 1
Essayer `lxc info --show-log local:abcdic4desktop` pour plus d'informations``
```
with that container log : `/var/log/lxd/abcdic4desktop/lxc.log`
```
lxc abcdic4desktop 20230926121120.535 ERROR    start - ../lxc-5.0.3/src/lxc/start.c:start:2197 - No such file or directory - Failed to exec "/sbin/init"
lxc abcdic4desktop 20230926121120.535 ERROR    sync - ../lxc-5.0.3/src/lxc/sync.c:sync_wait:34 - An error occurred in another process (expected sequence number 7)
lxc abcdic4desktop 20230926121120.538 WARN     network - ../lxc-5.0.3/src/lxc/network.c:lxc_delete_network_priv:3631 - Failed to rename interface with index 0 from "eth0" to its initial name "vetha0b16477"
lxc abcdic4desktop 20230926121120.538 ERROR    start - ../lxc-5.0.3/src/lxc/start.c:__lxc_start:2107 - Failed to spawn container "abcdic4desktop"
lxc abcdic4desktop 20230926121120.538 WARN     start - ../lxc-5.0.3/src/lxc/start.c:lxc_abort:1036 - No such process - Failed to send SIGKILL via pidfd 17 for process 19585
lxc abcdic4desktop 20230926121120.538 ERROR    lxccontainer - ../lxc-5.0.3/src/lxc/lxccontainer.c:wait_on_daemonized_start:878 - Received container state "ABORTING" instead of "RUNNING"
lxc 20230926121125.676 ERROR    af_unix - ../lxc-5.0.3/src/lxc/af_unix.c:lxc_abstract_unix_recv_fds_iov:218 - Connection reset by peer - Failed to receive response
lxc 20230926121125.676 ERROR    commands - ../lxc-5.0.3/src/lxc/commands.c:lxc_cmd_rsp_recv_fds:128 - Failed to receive file descriptors for command "get_state"
```
Note : same symptom : `start - ../lxc-5.0.3/src/lxc/start.c:start:2197 - No such file or directory - Failed to exec "/sbin/init"` as in rootless docker try, before delegating cgroupns with ` --cgroupns=private` in `docker run` command... Needs more investigation !
 
</details>
