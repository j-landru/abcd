# abcd : Alpine Based Cos (formally c3os latter kairos) Derivative
# Alpine derivative from (formally Cos/C3os/Elemental-tollkit, but now Kairos) immutable container image OS
#
# abcd4desktop : a Plamsa/KDE desktop derivative of abcd
# abcd4objectifipv6 an abcd4desktop derivative with gns3 tools for objectifipv6 MOOC labs
#

# 20230512 first version for abcd4objectifipv6
#ARG BASE_IMAGE=abcd4desktop:latest

# 20231214 first version for abcd4mowgli gns3server and lab standalone docker image so now abcd based
#ARG BASE_IMAGE=abcd:latest
## 20240119 from docker:dind with dockerd and gns3server side by side with supervisord see below
ARG BASE_IMAGE=docker:dind

FROM $BASE_IMAGE AS mowgli-base

#  packages for abcd4mowgli
RUN apk --no-cache add \
      bash \
      bash-completion \
      ubridge \
      wireshark \
      py3-pip \
      yarn \
      git \
      python3-dev \
      musl-dev \
      linux-headers \
      libcap-utils \
      dynamips
      
##      docker \
##      fuse-overlayfs
## 20240119 docker from raw install doesn't work in sysbox nested container  ??
##   dockerd launched, but unable to run alpine:latest image ?!! Not found howto make it work
##   abcdic4mowgli:/# docker run --rm -ti alpine:latest pwd
##   docker: Error response from daemon: failed to create task for container: failed to create shim task: OCI runtime create failed: runc create failed: unable to start 
##           container process: exec: "pwd": executable file not found in $PATH: unknown.
## ============= <docker.log file> ===============
## ...
## time="2024-01-19T14:55:14.783085759Z" level=info msg="shim disconnected" id=9cb8a84a6451184ef0729767a730976318936e03192531a90ea5983a84b2a33f namespace=moby
## time="2024-01-19T14:55:14.783144112Z" level=warning msg="cleaning up after shim disconnected" id=9cb8a84a6451184ef0729767a730976318936e03192531a90ea5983a84b2a33f namespace=moby
## time="2024-01-19T14:55:14.783152878Z" level=info msg="cleaning up dead shim" namespace=moby
## time="2024-01-19T14:55:14.789041472Z" level=warning msg="cleanup warnings time=\"2024-01-19T14:55:14Z\" level=warning msg=\"failed to read init pid file\" error=\"open /run/docker/containerd/daemon/io.containerd.runtime.v2.ta \
##  sk/moby/9cb8a84a6451184ef0729767a730976318936e03192531a90ea5983a84b2a33f/init.pid: no such file or directory\" runtime=io.containerd.runc.v2\n" namespace=moby
## time="2024-01-19T14:55:14.789315852Z" level=error msg="copy shim log" error="read /proc/self/fd/13: file already closed" namespace=moby
## time="2024-01-19T14:55:14.789708158Z" level=error msg="stream copy error: reading from a closed fifo
## ============= </docker.log file> ===============
##  
##   but reference docker:dind image works. So trying to derive 4mowgli image from sysbox:dind instead of abcd
##       and run dockerd and gns3server side by side in sysbox container ?? 
##
## so try to install as it is described in sysbox doc "Deploy a System Container with Systemd, sshd, and Docker inside"
##    https://github.com/nestybox/sysbox/blob/master/docs/quickstart/dind.md#deploy-a-system-container-with-systemd-sshd-and-docker-inside
##    dockerfile example : https://github.com/nestybox/dockerfiles/blob/master/ubuntu-bionic-systemd-docker/Dockerfile
##RUN curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh && \
##    # Add user "admin" to the Docker group
##    usermod -a -G docker admin && \
##    curl -L -o /etc/bash_completion.d/docker.sh https://raw.githubusercontent.com/docker/docker-ce/master/components/cli/contrib/completion/bash/docker
## sysbox doc "Deploy a System Container with Systemd, sshd, and Docker inside" doesn't work as get-docker.sh is not alpine friendly !!
##
## alternative : try docker:dind based and run dind dockerd and gns3 server side-by-side in container with supervisord ?!
##               https://github.com/nestybox/sysbox/blob/master/docs/quickstart/dind.md#deploy-a-system-container-with-supervisord-and-docker-inside
##               from dockerfile example : https://github.com/nestybox/dockerfiles/tree/master/alpine-supervisord-docker
##               note : docker and ssh already in docker:dind !!
# supervisord
RUN apk add --update supervisor && rm  -rf /tmp/* /var/cache/apk/* && \
    mkdir -p /var/log/supervisor
#COPY supervisord.conf /etc/
RUN cat >/etc/supervisord.conf <<EOF
; supervisord.conf file for side by side dockerd, sshd and gns3server
; abcd4mowgli : Mooc Objectif ipv6 Web based Gns3 Lab Instances

[supervisord]
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0

[program:dockerd]
command=/usr/bin/dockerd
; replace command with docker:dind entry point ??
priority=1
autostart=true
autorestart=true
startsecs=0

[program:sshd]
command=/usr/sbin/sshd -D
priority=1
autostart=true
autorestart=true
startsecs=0

; add here gns3-server stanza

EOF

#RUN addgroup kairos docker && addgroup docker root
#RUN addgroup docker root

# try gns3 isntall
# 20230512 trying manual install from Pypi see  https://docs.gns3.com/docs/getting-started/installation/linux/
# 20231010 error to install with pip3 :
#           error: externally-managed-environment
#           × This environment is externally managed
#           ╰─>
#            The system-wide python installation should be maintained using the system
#            package manager (apk) only.
#            
#            If the package in question is not packaged already (and hence installable via
#            "apk add py3-somepackage"), please consider installing it inside a virtual
#            environment, e.g.:
#            
#            python3 -m venv /path/to/venv
#            . /path/to/venv/bin/activate
#            pip install mypackage
#            
#            To exit the virtual environment, run:
#            
#            deactivate
#            
#            The virtual environment is not deleted, and can be re-entered by re-sourcing
#            the activate file.
#            
#            To automatically manage virtual environments, consider using pipx (from the
#            pipx package).
#        
#            note: If you believe this is a mistake, please contact your Python installation or OS distribution provider.
#                  You can override this, at the risk of breaking your Python installation or OS, by passing --break-system-packages.
#                  See PEP 668 for the detailed specification.  :  see https://peps.python.org/pep-0668/?ref=itsfoss.com
#            see also :   https://bbs.archlinux.org/viewtopic.php?id=286788
#                         https://fostips.com/pip-install-error-ubuntu-2304/


### 20231116 PyQt5 gns3 dependencies issue in Alpine 
### see https://stackoverflow.com/questions/76266695/cant-install-pyqt5-using-pip-on-alpine-docker
### unable install PyQt5 on Alpine
###
### RUN   mkdir /opt/GNS3 && \
###      python3 -m venv /opt/GNS3 && \
###      source /opt/GNS3/bin/activate && \
###      pip3 install --upgrade pip && \
###      pip3 install PyQt5 && \
###      pip3 install gns3-server && \
###      pip3 install gns3-gui
###
### 20231116 quick and dirty workaround and probably future dependencies issues...
### keep python isolation under /opt/GNS3 venv
### as staded in the above pip install PyQt5, gns3-server, gns3-gui inside debian/ubuntu like python:3.11 official contairer
### copy all that python stuff in the alpine /opt/GNS3 venv tree

#### 20231127 try install from source git cloning gns3-server
#### 20231127 libopengl dependencies for gns3-gui, unavailable inside container (abcdic), so try to use Gns3 webui
#### FROM python:3.11 AS gns3build
#### RUN apt update && apt install -y make automake gcc g++ subversion python3-dev gfortran libopenblas-dev && \
####     mkdir -p /opt/GNS3 && \
####     python3 -m venv /opt/GNS3 && \
####     cd /opt/GNS3 && \
####     . /opt/GNS3/bin/activate && \
####     pip install --upgrade pip && \
####     pip install psutil && \
####     pip install PyQt5 && \
####     pip install gns3-server && \
####     pip install gns3-gui
#### RUN echo -e "   inside gns-build after pip installs\n       -----oOo-----" > /opt/GNS3/inside-gns3-build.txt && \
####     echo -e "Is gns3 here ???\n=========== ls /opt/GNS3/bin ===========\n" >> /opt/GNS3/inside-gns3-build.txt && \
####     ls -al /opt/GNS3/bin  >> /opt/GNS3/inside-gns3-build.txt
#### 
#### FROM objectifipv6base AS objectifipv6final
#### 
#### 
#### WORKDIR /
#### #COPY --from=gns3build /opt/GNS3/bin /opt/GNS3/bin
#### #COPY --from=gns3build /opt/GNS3/lib /opt/GNS3/lib
#### #COPY --from=gns3build /opt/GNS3/inside-gns3-build.txt /opt/GNS3
#### COPY --from=gns3build /opt/GNS3 /opt/GNS3

#### 20231127 install gns"-server from sources (see GNS3 > Documentation > Contribute > Developpment of GNS3)
####  at https://docs.gns3.com/docs/contribute/development-of-gns3

#### 20231215 to have it work baremetal as well inside a container add ENV VIRTUAL_ENV=/opt/GNS3 and ENV PATH="$VIRTUAL_ENV/bin:$PATH"
####           see  "the elegant method" in https://pythonspeed.com/articles/activate-virtualenv-dockerfile/ for more details 

ENV VIRTUAL_ENV=/opt/GNS3
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN apk --no-cache add make automake gcc g++ subversion python3-dev gfortran openblas-dev && \
    mkdir -p /opt/GNS3 && mkdir -p /opt/GNS3-objectif-ipv6-labs && \
    echo -e "gns3 will be here !!!\n=========== ls /opt/GNS3 ===========\n" >> /opt/GNS3/gns3-trace.txt && \
    ls -al /opt/GNS3  >> /opt/GNS3/gns3-trace.txt >> /opt/GNS3/gns3-trace.txt && \
    python3 -m venv $VIRTUAL_ENV && \
    source /opt/GNS3/bin/activate && \
    echo -e " venv activated ???\n=========== ls /opt/GNS3 ===========\n" >> /opt/GNS3/gns3-trace.txt && \
    ls -al /opt/GNS3  >> /opt/GNS3/gns3-trace.txt >> /opt/GNS3/gns3-trace.txt && \
    cd /opt/GNS3 && \
    git clone https://github.com/GNS3/gns3-server.git && \
    cd /opt/GNS3/gns3-server && \
    pip install --upgrade pip && \
    pip3 install -r dev-requirements.txt && \
    py.test

### Arrgh !! /etc read-only file system, so moved to /etc/local.d/20-add-anyhost-entry-to-etchosts.start as connman recreat /etc/hosts  when "hostnamed".
###   py.test && \
###    echo -e "   add :: and 0.0.0.0 for anyhost name in /etc/hosts \n for gns3-server listening IPv6 and IPv4" >> /opt/GNS3/gns3-trace.txt && \
###    echo -e "   see host = anyhost stanza in /etc/xdg/GNS3/gns3_server.conf\n   from https://github.com/GNS3/gns3-server/issues/1673" >> /opt/GNS3/gns3-trace.txt && \
###    echo -e ":: anyhost\n0.0.0.0 anyhost" >> /etc/hosts
    
RUN echo -e "   inside gns-build after pip installs\n       -----oOo-----" > /opt/GNS3/inside-gns3-build.txt && \
    echo -e "Is gns3 here ???\n=========== ls /opt/GNS3/bin ===========\n" >> /opt/GNS3/inside-gns3-build.txt && \
    ls -al /opt/GNS3/bin  >> /opt/GNS3/inside-gns3-build.txt


#COPY abcd4mowgli files to /
COPY overlay/files/ / 
# set owners for /home subdirs
RUN chown -R 65535:65535 /home/kairos && chown -R 1000:100 /home/abcd && chown -R 1001:900 /home/wxyz
COPY overlay/files-alpine/ /
COPY overlay/files-iso /

# adjust python3 path in /opt/GNS3 venv (as Alpine python path differ from Debian like python:3.11 image ref !
RUN cd /opt/GNS3/bin && ln -sf /usr/bin/python3 python3 && \
    echo -e "Is gns3 here ???\n=========== ls /opt/GNS3/bin ===========\n" >> /opt/GNS3/gns3-trace.txt && \
    ls -al /opt/GNS3/bin  >> /opt/GNS3/gns3-trace.txt
    
## 20240119 4mowgli derived from docker:dind with supervisord, so not an abcd system container 
## but classical container so add entrypoint to set pid 1 to supervisord

RUN touch /usr/bin/4mowgli-entrypoint.sh && \
    chmod +x /usr/bin/4mowgli-entrypoint.sh && \
    cat >/usr/bin/4mowgli-entrypoint.sh <<EOF
#!/bin/sh
set -e

# sys container init:
#
# If no command is passed to the container, supervisord becomes init and
# starts all its configured programs (per /etc/supervisor/conf.f/supervisord.conf).
#
# If a command is passed to the container, it runs in the foreground;
# supervisord runs in the background and starts all its configured
# programs.
#
# In either case, supervisord always starts its configured programs.

if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
  exec supervisord -n "$@"
else
#  supervisord -c /etc/supervisor/conf.d/supervisord.conf &
  supervisord -c /etc/supervisord.conf &
  exec "$@"
fi

EOF

#####################ENTRYPOINT ["/usr/bin/4mowgli-entrypoint.sh"]
