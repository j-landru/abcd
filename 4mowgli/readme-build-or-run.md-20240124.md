# abcd4mogli build or run !!

###  !!! GNS3 gns3server need anyhost entries in /etc/hosts !!! 
 `:: anyhost` and `0.0.0.0 anyhost` in `/etc/hosts` needed for gns3 listenning dualstack IPv6 and IPv4
   otherwise gns3server won't start (and need to be run in debug mode to see what's wrong...)
 !!! but /etc/hosts exclusively managed by docker at runtime and is ro (immutable) !!!
 Arrgh !! /etc read-only file system, so moved to /etc/local.d/50-add-anyhost-entry-to-etchosts.start as connman recreat /etc/hosts  when "hostnamed".
```
 py.test && \
    echo -e "   add :: and 0.0.0.0 for anyhost name in /etc/hosts \n for gns3-server listening IPv6 and IPv4" >> /opt/GNS3/gns3-trace.txt && \
    echo -e "   see host = anyhost stanza in /etc/xdg/GNS3/gns3_server.conf\n   from https://github.com/GNS3/gns3-server/issues/1673" >> /opt/GNS3/gns3-trace.txt && \
    echo -e ":: anyhost\n0.0.0.0 anyhost" >> /etc/hosts
```
 ### 20240124
 4mowgli need to find a way to launch that /etc/local.d/50-add-anyhost-entry-to-etchosts.start at startup as mowgli is no more a system container but a supervisord container !!
 so use --add-host parm at "docker build" or "docker run" BUT unlike stated in docker doc (https://docs.docker.com/engine/reference/commandline/image_build/#add-host
 and https://docs.docker.com/engine/reference/commandline/container_run/#add-host) BUT !!! IPv6 bracket syntax don't wokr !!! 
 (see https://github.com/docker/cli/issues/4648) du to golang parser ?!
 so use "--add-host=anyhost::: --add-host=anyhost:0.0.0.0" in docker build or docker run for abcd4mowgli !!!

## To build abcd4mogli
```
docker build --no-cache --add-host=anyhost::: --add-host=anyhost:0.0.0.0  -t jlandru/abcd4mowgli -t abcd4mowgli .
```

## Or alterntively when launching abcd4mowgli 
```
docker run --runtime=sysbox-runc  --cgroupns=private --network=bridge --ulimit nofile=1024:1024 --hostname abcd4mowglirootless --name abcd4mowglirootless --add-host=anyhost::: --add-host=anyhost:0.0.0.0 --rm -ti -p 3888:3080 -p 6666:6000 jlandru/abcd4mowgli:latest /
```
