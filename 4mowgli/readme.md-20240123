# abcd4**mowgli** : abcd for  **M**ooc **O**bjectif ipv6 **W**eb  based **G**ns3 **L**ab **I**nstances. 

## idea ?! 

Since no success, (due to python constrictions of Qt dependencies), installing Gns3 heavyweight client (aka gns3-gui) in abcd4objectifipv6 and rolling back to gns3server embedded webinterface gui, I isolate  all MOOC IPv6 gns3 tools and labs here in a dedicated docker image for lab avalaible as container, or pod webservice as well as embedded on dekstop appliance : 

- GLOD  : **G**ns3 **L**ab web service **O**n **D**emand : abcdic4mowgli run inside stand alone container or at scale on cloud in k8s pod ;
- GL(OD)2 : **G**ns3 **L**ab web service **O**n **D**emand **O**n **D**esktop : abcdic4mowgli image embedded inside abcd4objectifipv6 desktop appliance for local lab session on abcdic4objectifip6 desktop container or abcdik4objectifipv6 desktop liveiso baremetal/vm.


## nested virtualization context
GNS3 models run simulated network node (switch, router, host, server, ...), usually in fully virtualized VM (kvm hypervisor) or inside containers. This virtualization GNS3 execution context (second virtualization level) is run inside the 4objectifipv6 appliance itself virtualized (first virtualization level). This version of the appliance leave vyos kvm VM router image, used previously in objectif ipv6 labs, to switch to docker one. So this abcd instance needs to embed GNS3 simulator and a docker execution context. 

nested virtualisation is :
- Docker node inside HVM on abcd**ik**4objectifipv6 appliance (rootful gns3 docker nodes confined inside HVM VM) (see GL(OD)2 above)
- Docker inside rootless container (DinD or [sysbox](https://github.com/nestybox/sysbox) on abcd**ic**4objectifipv6 appliance (see GLOD or GL(OD)2 above)

## questions and issues

- Vyos docker image 
 -- build following guidelines from Vyos doc reference : "[Deploy container from ISO](https://docs.vyos.io/en/latest/installation/virtual/docker.html)" from [vyos-rolling-nightly-builds](https://github.com/vyos/vyos-rolling-nightly-builds/releases/) (v 1.5-rolling-20231080024 a time of writing)
 -- to reduce image size, vyos image, plan to build from abcd4mowgli dockerfile multi-stage build, and import vyos docker image inside abcdic4mowgli docker image, but unable to run "docker import" at "docker build" time. So workaround was to primarly build vyos docker image on host then export vyos image as well other images needed inside abcd4mowgli rootfs to /tmp/img-store then start abcdic4mowgli in rootfull container instance with volume `-v /tmp/img-store:/tmp/img-store` and `-v overlay/files/var/lib/docker:/var/lib/docker:rw` to import images (see below)
- running rootful vyos container (needed by GNS3) inside rootless abcdic instance ??  Howto ?? DinD or sysbox ??
- GNS3 state snapshots for step-by-step successive lab immutable states ?? "docker save" for successive image layers ?? tag named images ??

## pre loading vyos docker images (and others) inside abcd4mowgli image

Gns3 inside abcd4mowgli use docker images (vyos, alpine, ...), these images need to be present (preloaded/imported) in local docker repository (`/var/lib/docker`) because we are unable to run "docker import" at "abcd4mowgli build" time. So workaround import vyos image on abcd4mowgli overlay tree which will be copied at buildtime.

### primarly build vyos docker image on host and export vyos image as well other images needed inside abcd4mowgli rootfs as archive

On host, build vyos docker image following guidelines from Vyos doc reference : "[Deploy container from ISO](https://docs.vyos.io/en/latest/installation/virtual/docker.html)" from [vyos-rolling-nightly-builds](https://github.com/vyos/vyos-rolling-nightly-builds/releases/) (v 1.5-rolling-202312080024 a time of writing so adjust to latest relase when building) and save it as an archive, which will be imported inside the abcd4mowgli image.
```
# cd /tmp/
# mkdir vyos && cd vyos
# # unlike vyos guideline, don't miss '-L' parm, as now vyos-rolling-nightly-build url is redirected !!
# # retrieving URL, TAG and DWNLD_URL of latest Vyos version
# export VYOS_LATEST_URL=$(curl -v -L https://github.com/vyos/vyos-rolling-nightly-builds/releases/latest 2>&1 | grep -i  "< location:" | sed -n 's/.*\(http[s]\?:\/\/[^ ]\+\).*/\1/p')
# echo "Latest Vyos : "${VYOS_LATEST_URL}
# export VYOS_LATEST_TAG=$(echo ${VYOS_LATEST_URL##*/})
# echo "Latest Vyos tag : "${VYOS_LATEST_TAG}
# # download latest Vyos iso file
# export VYOS_LATEST_ISO_DWNLD=$(echo "https://github.com/vyos/vyos-rolling-nightly-builds/releases/download/${VYOS_LATEST_TAG%$'\r'}/vyos-${VYOS_LATEST_TAG%$'\r'}-amd64.iso")
# echo "Latest Vyos iso download url : "${VYOS_LATEST_ISO_DWNLD}
# # download iso file
# curl -L -o vyos-${VYOS_LATEST_TAG%$'\r'}-amd64.iso ${VYOS_LATEST_ISO_DWNLD%$'\r'}
# # download minisign file
# curl -L -o vyos-${VYOS_LATEST_TAG%$'\r'}-amd64.iso.minisig ${VYOS_LATEST_ISO_DWNLD%$'\r'}.minisig
# # download minisign Vyos actual pub key
# curl -L -o minisign.pub https://raw.githubusercontent.com/vyos/vyos-rolling-nightly-builds/main/minisign.pub
# # signature verification of nightly build !!!
# minisign -Vm vyos-${VYOS_LATEST_TAG%$'\r'}-amd64.iso
# mkdir rootfs
# mkdir unsquashfs
# mount -o loop vyos-${VYOS_LATEST_TAG%$'\r'}-amd64.iso rootfs
# unsquashfs -f -d unsquashfs/ rootfs/live/filesystem.squashfs
# tar -C unsquashfs -c . | docker import - vyos:${VYOS_LATEST_TAG%$'\r'} vyos:latest
# docker images
# # tag 1.x-rolling-aaaammjjnnnn as vyos:latest
# docker tag vyos:${VYOS_LATEST_TAG%$'\r'} vyos:latest
# mkdir /tmp/img-store
# # save image as tar archive
# docker save -o "/tmp/img-store/vyos-${VYOS_LATEST_TAG%$'\r'}.tar" vyos:${VYOS_LATEST_TAG%$'\r'}
# # set read rights
# chmod 644 /tmp/img-store/vyos-${VYOS_LATEST_TAG%$'\r'}.tar
# ls -alh /tmp/img-store/
# docker pull alpine:latest
# docker images
# docker save -o "/tmp/img-store/alpine-$(date +%Y%m%d).tar" alpine:latest
# # set read rights again
# chmod 644 /tmp/img-store/alpine-$(date +%Y%m%d).tar 
#  ls -alh /tmp/img-store/
```

### import archive inside a rootful abcdic4mowgli running instance with local docker image repository volumed on the overlay dir
then start an ephemeral abcdic4mowgli container as a rootfull container instance with host networking and with volume `-v /tmp/img-store:/tmp/img-store` and `-v <path-abcd4mowgli-overlay>/files/var/lib/docker:/var/lib/docker:rw` to import vyos image and pull alpine image inside local repository. Goal is to populate `/var/lib/dokcer `of the overlaydir, which will be copied inside abcd4mowgli image at  build time. 
Launch abcdic4mowgli with volumes pointing to the rights host dirs (need rootful mode, aka privileged so if you container execution context is isolated with user namespace (usually recommanded), switch back temporarly your docker env to rootful mode before  running that abcdic4mowgli instance.

### 20240123 no more abcdic4mowgli as abcd4mogli now a servce container driven by supervisord !!
### ```
### # docker run --privileged --cgroupns=private --ulimit nofile=1024:1024 --network=host --hostname abcdic4mowgli --name abcdic4mowgli --rm -ti \
###   -v /rootfs-store/abcd/4mowgli/overlay/files/var/lib/docker:/var/lib/docker:rw \
###   -v /tmp/img-store:/tmp/img-store \
###   jlandru/abcdic4mowgli:latest
### ```

```
# docker run --privileged --cgroupns=private --ulimit nofile=1024:1024 --network=host --hostname abcdic4mowgli --name abcdic4mowgli -ti \
  -v /rootfs-store/abcd/4mowgli/overlay/files/var/lib/docker:/var/lib/docker:rw \
  -v /tmp/img-store:/tmp/img-store \
  jlandru/abcdic4mowgli:latest "/usr/bin/dockerd"
```
Go inside that container
```
# docker exec -ti abcdic4mowgli /bin/bash
```

Import and tag the images

```
abcd4mowgli:/# # list initial state of docker local repository (normally empty)
abcd4mowgli:/# docker images
abcd4mowgli:/# # import vyos and alpine images build above from /tmp/image-store
abcd4mowgli:/# # list archive images from /tmp/img-store
abcd4mowgli:/# ls -alh /tmp/img-store
abcd4mowgli:/# # extract vyos and alpine tag from archive names !!!!
abcd4mowgli:/# export VYOS_LATEST_NAME=$(echo $(basename -s ".tar" $(ls -t /tmp/img-store/vyos* | head -1)))
abcd4mowgli:/# echo "Latest vyos name : "${VYOS_LATEST_NAME}
abcd4mowgli:/# export VYOS_LATEST_ALT_TAG=$(echo ${VYOS_LATEST_NAME##*vyos-})
abcd4mowgli:/# echo "Latest vyos alt tag : "${VYOS_LATEST_ALT_TAG}
abcd4mowgli:/# export ALPINE_LATEST_NAME=$(echo $(basename -s ".tar" $(ls -t /tmp/img-store/alpine* | head -1)))
abcd4mowgli:/# echo "Latest alpine name : "${ALPINE_LATEST_NAME}
abcd4mowgli:/# export ALPINE_LATEST_ALT_TAG=$(echo ${ALPINE_LATEST_NAME##*alpine-})
abcd4mowgli:/# echo "Latest alpine alt tag : "${ALPINE_LATEST_ALT_TAG}
abcd4mowgli:/# docker images
abcd4mowgli:/# # normally empty !!
abcd4mowgli:/# # now load (and not import !!)import vyos image
abcd4mowgli:/# docker load -i /tmp/img-store/${VYOS_LATEST_NAME}.tar
abcd4mowgli:/# # tag to vyos:latest
abcd4mowgli:/# docker tag vyos:${VYOS_LATEST_ALT_TAG} vyos:latest
abcd4mowgli:/# # now load (and not import !!!) alpine image
abcd4mowgli:/# docker load -i /tmp/img-store/${ALPINE_LATEST_NAME}.tar
abcd4mowgli:/# # tag to alpine:latest-alt-tag
abcd4mowgli:/# docker tag alpine:latest alpine:${ALPINE_LATEST_ALT_TAG}

abcd4mowgli:/# # list your local repository content before exiting
abcd4mowgli:/# docker images

REPOSITORY   TAG                        IMAGE ID       CREATED          SIZE
alpine       aaaammjj                   bc49e90c2ad8   29 seconds ago   7.67MB
alpine       latest                     bc49e90c2ad8   29 seconds ago   7.67MB
vyos         1.x-rolling-aaaammjjnnnn   3c1074436d90   4 minutes ago    1.53GB
vyos         latest                     3c1074436d90   4 minutes ago    1.53GB
abcd4mowgli:/# exit
exit
```
On host Stop and remove abcd4mowgli container and verify ovelrlay dir content

```
# docker stop abcd4mowgli && docker rm abcd4mowgli
# du -sh /rootfs-store/abcd/4mowgli/overlay/files/var/lib/docker/
# tree -L 4 /rootfs-store/abcd/4mowgli/overlay/files/var/lib/docker/
```

It's now time to : 
a) switch back your docker execution context from rootful mode (privileged) to its usual rootless mode (isolated with user namespace)
b) rebuild abcd4mowgli to embed vyos and alpine images inside mowgli image from overlaydir (when copying that latter see 4mowgli dockerfile). rebuild also 4mogli son aka abcdic4mowgli, then push abcd4mowgli and abcdic4mowgli images to your container registry

```
# docker push  <your-regsitry>/abcd4mowgli:latest
# docker push  <your-regsitry>/abcdic4mowgli:latest
```

For GL(OD)2 : you're now ready to rebuild abcd4objectifipv6, abcdic4objectifipv6 and abcdik4objectifipv6 images for embedding 4mowgli container image using the same way used here to embed alpine image inside abcd4mowgli.

### see also

- [https://www.baeldung.com/linux/install-docker-alpine-container](https://www.baeldung.com/linux/install-docker-alpine-container)
- [sysbox](https://github.com/nestybox/sysbox)
