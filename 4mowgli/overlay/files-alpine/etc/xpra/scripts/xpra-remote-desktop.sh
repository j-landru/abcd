#!/bin/bash

# At each session, sddm restarts Xorg and renews  xauth file  under /var/run/sddm to display greeter so need to adjust XAUTHORITY env before launching xpra upgrade
#  xpra always launched in upgrade-destop mode
#  bind-tcp localhost - remote access through ssh tunnel -L 8080:[::1]:8080 or ancient IPV4 -L 8080:127.0.0.1:8080 
#
# 20230712 for more conventinal secure access such https, wss or quic consider --bind-tcp=*:8080
#          see https://github.com/orgs/Xpra-org/discussions/3911 for more details
#
# adjust XAUTHORITY before xpra upgrade-desktop
## for test and debug
##echo "before xpra launch /var/run/sddm dir  : "/var/run/sddm/$(ls -t /var/run/sddm) " --XAUTHORITY="$XAUTHORITY > /var/log/xauthority-env.txt
##  when launched inside /usr/share/sddm/scripts/Xsetup at Xorg startup several xauth files (previous and new) are present under /var/run/sddm so set XAUTHORITY env to the newest one using "ls -t" and filter the first with "head -n 1" 

export XAUTHORITY=/var/run/sddm/$(ls -t /var/run/sddm | head -n 1) && \
       /usr/bin/xpra upgrade-desktop :0 --bind-tcp=[::1]:8080 --bind-tcp=[::ffff:127.0.0.1]:8080 \
       --readonly=no --sharing=yes 2>&1 > /var/log/xpra-remote-desktop-sh.log

       
## for test and debug
##echo "after  xpra launch /var/run/sddm dir  : "/var/run/sddm/$(ls -t /var/run/sddm) " -- XAUTHORITY="$XAUTHORITY >> /var/log/xauthority-env.txt
