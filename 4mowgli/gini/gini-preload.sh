#!/bin/sh
set -e

### vyos, alpine, and other images pre-loading into  abcd4mowgli system container
###     see sysbox docs titled : Sysbox Quick Start Guide: Preloading Inner Container Images into System Containers
###                     https://github.com/nestybox/sysbox/blob/master/docs/quickstart/images.md

# busybox basename command syntax diverges from basnename classical
# so install coreutils
#apk add --no-cache docker coreutils 

# dockerd start
dockerd > /var/log/dockerd.log 2>&1 &
sleep 2


# is docker running ?!
echo -e "=== IS DOCKER RUNNING ===="
ps -edf
echo -e "==== DOCKER INFO ===="
docker info
echo -e "=== AFTER IS DOCKER RUNNING ===="


### unlike sysbox preloading howto, instead of pulling image from public respository
###  load local pre build and archived images (see minima subdir).
### # pull inner images
### docker pull alpine:latest


# load pre build objectifipv6  vyos, alpine, and others... from minima subdir.

# extract vyos and alpine tag from archive names !!!!
echo "img-store content : ls -alsh /tmp/img-store : " && ls -alsh /tmp/img-store
export VYOS_LATEST_NAME=$(echo -e $(basename -s ".tar" $(ls -t /tmp/img-store/vyos* | head -1)))
echo "Latest vyos name : " ${VYOS_LATEST_NAME}
export VYOS_LATEST_ALT_TAG=$(echo -e ${VYOS_LATEST_NAME##*vyos-})
echo "Latest vyos alt tag : "${VYOS_LATEST_ALT_TAG}
export ALPINE_LATEST_NAME=$(echo -e $(basename -s ".tar" $(ls -t /tmp/img-store/alpine* | head -1)))
echo "Latest alpine name : "${ALPINE_LATEST_NAME}
export ALPINE_LATEST_ALT_TAG=$(echo -e ${ALPINE_LATEST_NAME##*alpine-})
echo "Latest alpine alt tag : "${ALPINE_LATEST_ALT_TAG}

## verify iniital content of /var/lib/docker tree
echo "initial state of  /var/lib/docker"
ls -alh /var/lib/docker
du -d 1 -h /var/lib/docker

# now loads (and not import !!)import vyos image
docker image load --help
docker load -i /tmp/img-store/${VYOS_LATEST_NAME}.tar
## verify
docker images
## add default "latest" tag to vyos
docker tag vyos:${VYOS_LATEST_ALT_TAG} vyos:latest
## verify again
docker images

echo "state of  /var/lib/docker after loading inner vyos image"
ls -alh /var/lib/docker
du -d 1 -h /var/lib/docker


# now loads (and not import !!!) alpine image
docker load -i /tmp/img-store/${ALPINE_LATEST_NAME}.tar
## verify
docker images
## add  "aaaammjj" alt tag
docker tag alpine:latest alpine:${ALPINE_LATEST_ALT_TAG}
## list your local repository content before exiting
docker images
## display should look like that... 
## REPOSITORY   TAG                        IMAGE ID       CREATED          SIZE
## alpine       aaaammjj                   bc49e90c2ad8   29 seconds ago   7.67MB
## alpine       latest                     bc49e90c2ad8   29 seconds ago   7.67MB
## vyos         1.x-rolling-aaaammjjnnnn   3c1074436d90   4 minutes ago    1.53GB
## vyos         latest 

echo "state of  /var/lib/docker after loading inner alpine image"
ls -alh /var/lib/docker
du -d 1 -h /var/lib/docker

# now loads (not imports!!!) any other gns3 lab node images
# like abcd4wasabi to display wireshark capture inside browser...

echo "final state of  /var/lib/docker"
ls -alh /var/lib/docker
du -d 1 -h /var/lib/docker


# dockerd cleanup (remove the .pid file as otherwise it prevents
# dockerd from launching correctly inside sys container)
kill $(cat /var/run/docker.pid)
kill $(cat /run/docker/containerd/containerd.pid)
rm -f /var/run/docker.pid
rm -f /run/docker/containerd/containerd.pid
