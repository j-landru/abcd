elemental-toolkit docker image running inside docker : abcd4vind
================================================================


20221220 Notes to reproduce simultenous abcd4viminal inside libvirt/KVM VM and inside Docker (see screenshot simultaneous-abcd4vind-and-abcd4viminal-202212205-359x1440.png !!)

- start docker on hlandru and log on on dockerhub

 # systemctl start docker
 # docker login
   ...
   Login Succeeded
 # docker images

- container image abcd4vind derivative from abcd4viminal :  see Dockerfile.abcd4vind.sav20221216

==building abcd4viminal==

 # docker build -t abcd4viminal -t jlandru/abcd4viminal .
 # docker images
 ## docker push jlandru/abcd4viminal
 # push image on docker hub
 ## build elemental-toolkit iso (see elemental toolkit documentation
 # docker run --rm -ti  -v $(pwd):/build quay.io/costoolkit/elemental-cli:v0.0.14-e4e39d4 \
   --debug --config-dir /build  build-iso --logfile /build/iso-build.log \
   -o /build jlandru/abcd4viminal:latest
 ## move image to /rootfs-store/cirrus-os
 # mv Elemental-0-abcd4viminal.20221212.iso ../..
 ## adjust iso image in virt-mamager then run VM

 
 ==building abacd4vind==
 
  ## buld abcd4vind docker image
  # docker build -t abcd4vind -t jlandru/abcd4vind -f ./Dockerfile.abcd4vind.sav20221216 .
  ## push it on dockerhub
  # docker push jlandru/abcd4vind
  
  
== run abcd4vind inside Docker

  ## launch it
  # docker run -it --name abcd4vind -p:8080:8080 abcd4vind
  # login in usig docker exec (no docker attach !! be careful
  # docker exec -it abcd4vind /bin/bash
  ## set root password
  # password
  ## modify /etc/ssh/sshd-config to allow root login for test !!
  ## restart sshd
  # /etc/init.d/ssh restart
  ## you can the ssh login inside the container ssh root@172.17.0.2
  
== plasma desktop ==
 
 ## plasma desktop (and sddm inside the VM) need /etc/init.d/elogind-cgroup ; /etc/init.d/elogind and /etc/init.d/utmpd started
 ## manually launch (need to be added in the root runlevel
 
* inside the VM : login c3os/c3os and start sddm
 # /etc/init.d/sddm start
 
** Nota : on some hardware (live iso on baremetal laptop, kde/plasmadesktop has an issue plasma is ok on fullscreen but despktop is reduce (less than 25% on top left corner) in that case we need to restart plasmashell (need to find a way to force it through sddm autologin ?) with that command
 # plasmashell --replace >/dev/null 2>&1 &
 
* inside the docker container : xrpa instead of xorg
  ## launch xpra and bind to 0.0.0.0 on port 8080 (for display :8080)
  #  xpra start-desktop :8080 --resize-display=1080x1024 \
     --start-child="startplasma-x11" --exit-with-children \
     --bind-tcp=0.0.0.0:8080

point ypur browser on localhost:8080   => http://172.17.0.2:8080/#


 

  
 
  
  
