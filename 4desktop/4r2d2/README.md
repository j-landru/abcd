# abcd4**r2d2** : abcd for **R**os**2** **D**igitalized twin **D**evice

## ROS2 image

### idea ?!  : to run digitalized twin robots, on a VM or pod hosted in a k3s edge cloud, side by side with real robots


### Questions : 
- ROS2 with or without desktop ? r2d2 son of abcd on former case, daughter of abcd4desktop on latter
- which distro ? 
  -- ROS2 on Alpine (abcd4r2d2) ?
  -- or native Ubuntu ROS2, needs to dig ubcd (Ubuntu Based Cos Derivative) for ubcd4r2d2 ?
- identical or fraternal twins ? abcd on arm in the 1st case, arm real robot and x86_64 virtualized twin in the second

