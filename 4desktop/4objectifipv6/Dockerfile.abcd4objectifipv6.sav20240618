# abcd : Alpine Based Cos (formally c3os latter kairos) Derivative
# Alpine derivative from (formally Cos/C3os/Elemental-tollkit, but now Kairos) immutable container image OS
#
# abcd4desktop : a Plamsa/KDE desktop derivative of abcd
# abcd4objectifipv6 an abcd4desktop derivative with gns3 tools for objectifipv6 MOOC labs
#

#20230512 first version for abcd4objectifipv6
ARG BASE_IMAGE=abcd4desktop:latest

FROM $BASE_IMAGE AS objectifipv6base

#  desktop packages for abcd4objectifipv6
#  /usr/bin/update-desktop-database needed by "gns3-webclient-config --install-mime-types"
#      so install destkop-file-utils !!
RUN apk --no-cache add \
      ubridge \
      wireshark \
      busybox-extras \
      py3-pip \
      yarn \
      git \
      python3-dev \
      musl-dev \
      linux-headers \
      libcap-utils \
      dynamips \
      docker \
      fuse-overlayfs \
      desktop-file-utils
      

#  add abcd and wxyz to docker group allowing those user to run docker containers
RUN addgroup abcd docker && addgroup wxyz docker
#  add abcd and wxyz to wireshark group allowing network capture from GNS3
RUN addgroup abcd wireshark && addgroup wxyz wireshark


#RUN addgroup kairos docker && addgroup docker root
#RUN addgroup docker root


#*******************************   About GNS3 in Alpine Linux :    **********************************
#*
#*  No GNS3 package avalaible for Alpine so intall all stuff from github/GNS3 latest stable release by git cloning
#*  GNS3 is a huge python code in 3 parts : 
#*          - gns3-server :  the GNS3 lab runtinme (installed here but contenairized in abcd4mowgli in future),
#*          - gns3-gui : the heavy client allowing project lab creation and run 
#*          - gns3-webclient : GNS3 lab run by gns3server can be displayed and run from gns3-gui and browser, but in that latter case
#*                 GNS3 is not yet a fully web app as tool like wireshark or telnet remote node access
#*                 have to be run locally on desktop not in browser. gns3-webclient pack is the glue to launch local wireshark
#*                 or telnet client from gns3 lab accessed from browser
#*  Each part is installed in its own python vitual env (venv) for better modularity and isolation
#*
#*  DISCLAIMER : gns3-gui has to be run rootless, doesn't work when launched with super user rights nor when delegated by sudo,
#*            So if it doesn't work let first assume your are in the right python venv (activate) with user rights !!!
#*
#*  DISCLAIMER : gns3-gui hangs when network config is managed by native kairos connman tool
#*              even strace doesn't clearly show what happen (hangs appeasr during /usr/lib/qt5/plugins/bearer/libqnmbearer.so call)
#*              see : "localhost:/opt/gns3-gui$ strace /opt/gns3-gui/bin/python3 -m gns3 --debug"
#*              Workaround to make gns3 work : switch network manager from connman to networkmanager
#*              /etc/init.d/connman stop && /etc/init.d/networkmanager start
#*              Nota : under KDE, networkmanager is better integrated than connman, 
#*                     but hope no more other dependency issues leaving connman with kairos ??!!
#*                     so abcd4objectifipv6 depends on an abcd4desktop switched networkmanager than connman
#*

## install common dependencies PyQt at python system runlevel using py3qt5 package
RUN apk --no-cache add \
      py3-qt5


# try gns3 isntall
# 20230512 trying manual install from Pypi see  https://docs.gns3.com/docs/getting-started/installation/linux/
# 20231010 error to install with pip3 :
#           error: externally-managed-environment
#           × This environment is externally managed
#           ╰─>
#            The system-wide python installation should be maintained using the system
#            package manager (apk) only.
#            
#            If the package in question is not packaged already (and hence installable via
#            "apk add py3-somepackage"), please consider installing it inside a virtual
#            environment, e.g.:
#            
#            python3 -m venv /path/to/venv
#            . /path/to/venv/bin/activate
#            pip install mypackage
#            
#            To exit the virtual environment, run:
#            
#            deactivate
#            
#            The virtual environment is not deleted, and can be re-entered by re-sourcing
#            the activate file.
#            
#            To automatically manage virtual environments, consider using pipx (from the
#            pipx package).
#        
#            note: If you believe this is a mistake, please contact your Python installation or OS distribution provider.
#                  You can override this, at the risk of breaking your Python installation or OS, by passing --break-system-packages.
#                  See PEP 668 for the detailed specification.  :  see https://peps.python.org/pep-0668/?ref=itsfoss.com
#            see also :   https://bbs.archlinux.org/viewtopic.php?id=286788
#                         https://fostips.com/pip-install-error-ubuntu-2304/


### 20231116 PyQt5 gns3 dependencies issue in Alpine 
### see https://stackoverflow.com/questions/76266695/cant-install-pyqt5-using-pip-on-alpine-docker
### unable install PyQt5 on Alpine
###
### RUN   mkdir /opt/gns3 && \
###      python3 -m venv /opt/gns3 && \
###      source /opt/gns3/bin/activate && \
###      pip3 install --upgrade pip && \
###      pip3 install PyQt5 && \
###      pip3 install gns3-server && \
###      pip3 install gns3-gui
###
### 20231116 quick and dirty workaround and probably future dependencies issues...
### keep python isolation under /opt/gns3 venv
### as staded in the above pip install PyQt5, gns3-server, gns3-gui inside debian/ubuntu like python:3.11 official contairer
### copy all that python stuff in the alpine /opt/gns3 venv tree

#### 20231127 try install from source git cloning gns3-server
#### 20231127 libopengl dependencies for gns3-gui, unavailable inside container (abcdic), so try to use Gns3 webui
#### FROM python:3.11 AS gns3build
#### RUN apt update && apt install -y make automake gcc g++ subversion python3-dev gfortran libopenblas-dev && \
####     mkdir -p /opt/gns3 && \
####     python3 -m venv /opt/gns3 && \
####     cd /opt/gns3 && \
####     . /opt/gns3/bin/activate && \
####     pip install --upgrade pip && \
####     pip install psutil && \
####     pip install PyQt5 && \
####     pip install gns3-server && \
####     pip install gns3-gui
#### RUN echo -e "   inside gns-build after pip installs\n       -----oOo-----" > /opt/gns3/inside-gns3-build.txt && \
####     echo -e "Is gns3 here ???\n=========== ls /opt/gns3/bin ===========\n" >> /opt/gns3/inside-gns3-build.txt && \
####     ls -al /opt/gns3/bin  >> /opt/gns3/inside-gns3-build.txt
#### 
#### FROM objectifipv6base AS objectifipv6final
#### 
#### 
#### WORKDIR /
#### #COPY --from=gns3build /opt/gns3/bin /opt/gns3/bin
#### #COPY --from=gns3build /opt/gns3/lib /opt/gns3/lib
#### #COPY --from=gns3build /opt/gns3/inside-gns3-build.txt /opt/gns3
#### COPY --from=gns3build /opt/gns3 /opt/gns3


#### 20231127 install gns"-server from sources (see GNS3 > Documentation > Contribute > Developpment of GNS3)
####  at https://docs.gns3.com/docs/contribute/development-of-gns3

#****************************  GNS3 parth I : gns3-server  ***********************

# 20240618  OK resume clean process   ## 20240516 test not working (DinD ?) so bypass test - manually try test in abcdic4objectifipv6 ?
# 20240618  OK resume clean process   ## 20240530 gns3 docker nodes need busybox present inside PATH of GNS3 venv see https://github.com/GNS3/gns3-gui/issues/3569
# 20240618  OK resume clean process   ##          so add symbolic links of /bin/busybox and /bin/busybox-extra in /root/.local/shre/GNS3/docker/resources/bin/
# 20240618  OK resume clean process   ##          as gns3server run as root in that abcd4objectifipv6 sandbox
# 20240618  OK resume clean process   RUN apk --no-cache add make automake gcc g++ subversion python3-dev gfortran openblas-dev && \
# 20240618  OK resume clean process       mkdir -p /opt/gns3 && \
# 20240618  OK resume clean process       echo -e "gns3 will be here !!!\n=========== ls /opt/gns3 ===========\n" >> /opt/gns3/gns3-trace.txt && \
# 20240618  OK resume clean process       ls -al /opt/gns3  >> /opt/gns3/gns3-trace.txt >> /opt/gns3/gns3-trace.txt && \
# 20240618  OK resume clean process       python3 -m venv /opt/gns3 && \
# 20240618  OK resume clean process       source /opt/gns3/bin/activate && \
# 20240618  OK resume clean process       echo -e " venv activated ???\n=========== ls /opt/gns3 ===========\n" >> /opt/gns3/gns3-trace.txt && \
# 20240618  OK resume clean process       ls -al /opt/gns3  >> /opt/gns3/gns3-trace.txt >> /opt/gns3/gns3-trace.txt && \
# 20240618  OK resume clean process       cd /opt/gns3 && \
# 20240618  OK resume clean process       git clone https://github.com/GNS3/gns3-server.git && \
# 20240618  OK resume clean process       cd /opt/gns3/gns3-server && \
# 20240618  OK resume clean process       pip install --upgrade pip && \
# 20240618  OK resume clean process       pip3 install -r dev-requirements.txt && \
# 20240618  OK resume clean process       mkdir -p /root/.local/share/GNS3/docker/resources/bin/ && \
# 20240618  OK resume clean process       ln -s /bin/busybox /root/.local/share/GNS3/docker/resources/bin/busybox && \
# 20240618  OK resume clean process       ln -s /bin/busybox-extras /root/.local/share/GNS3/docker/resources/bin/busybox-extras
# 20240618  OK resume clean process       
# 20240618  OK resume clean process   ##    pip3 install -r dev-requirements.txt && \
# 20240618  OK resume clean process   ##    py.test
# 20240618  OK resume clean process   
# 20240618  OK resume clean process   RUN echo -e "   inside gns-build after pip installs\n       -----oOo-----" > /opt/gns3/inside-gns3-build.txt && \
# 20240618  OK resume clean process       echo -e "Is gns3 here ???\n=========== ls /opt/gns3/bin ===========\n" >> /opt/gns3/inside-gns3-build.txt && \
# 20240618  OK resume clean process       ls -al /opt/gns3/bin  >> /opt/gns3/inside-gns3-build.txt
# 20240618  OK resume clean process  ##* # adjust python3 path in /opt/gns3 venv (as Alpine python path differ from Debian like python:3.11 image ref !
# 20240618  OK resume clean process  ##* RUN cd /opt/gns3/bin && ln -sf /usr/bin/python3 python3 && \
# 20240618  OK resume clean process  ##*     echo -e "Is gns3 here ???\n=========== ls /opt/gns3/bin ===========\n" >> /opt/gns3/gns3-trace.txt && \
# 20240618  OK resume clean process  ##*     ls -al /opt/gns3/bin  >> /opt/gns3/gns3-trace.txt

# 20240618  OK resume clean process
#   - git clone gns3-server from github
#   - create gns3-server pyhton venv and activate
#   - recursively pip install
#   - make busybox links as stated in the 20240530 note above
#   - deactivate

RUN apk --no-cache add gcc python3-dev musl-dev linux-headers && \
    cd /opt && \
    git clone https://github.com/GNS3/gns3-server.git && \
    python3 -m venv --system-site-packages /opt/gns3-server && \
    source /opt/gns3-server/bin/activate && \
    cd /opt/gns3-server && \
    pip install --upgrade pip && \
    pip3 install -r dev-requirements.txt && \
    mkdir -p /root/.local/share/GNS3/docker/resources/bin/ && \
    ln -s /bin/busybox /root/.local/share/GNS3/docker/resources/bin/busybox && \
    ln -s /bin/busybox-extras /root/.local/share/GNS3/docker/resources/bin/busybox-extras && \
    deactivate


#****************************  GNS3 parth II : gns3-gui  ***********************
# 20240618  OK resume clean process
#   - git clone gns3-gui from github
#   - create gns3-gui pyhton venv and activate
#   - recursively pip install
#   - deactivate

RUN cd /opt && \
    git clone https://github.com/GNS3/gns3-gui.git && \
    python3 -m venv --system-site-packages /opt/gns3-gui && \
    source /opt/gns3-gui/bin/activate && \
    cd /opt/gns3-gui && \
    pip install --upgrade pip && \
    pip3 install -r dev-requirements.txt && \
    deactivate    

#****************************  GNS3 parth III : gns3-webclient  ***********************
## 20240517 try to manually install gns3-webclient-package
##RUN cd /opt/gns3 && \
##    source /opt/gns3/bin/activate && \
##    python3 -m pip install gns3-webclient-pack && \
##    gns3-webclient-config --install-mime-types
## Arrgghh ! "gns3-webclient-config --intall" analog problematic Qt dedencies issue
## than when initialy trying to install gns3 heavy classical  client
##  error : "Can't import Qt modules: Qt and/or PyQt is probably not installed correctly..."
## try "gns3-webclient-config --install-mime-types" directly from desktop when running abcd kde destop :
##RUN cd /opt/gns3 && \
##    source /opt/gns3/bin/activate && \
##    python3 -m pip install gns3-webclient-pack
## 20240610 As it took me a while to install and run gns3-webclient read that note !!
##      - gns3-webclient-pack is installed and run inside its own python venv with --system-site-packages 
##        as PyQt versions are alpine package installed
##      - Alpine is now Qt6 with Qt5 compat so PyQt is installed in both version (py3-qt6 and py3-qt5 apk packages)
##      - gns3-webclient doasn't work with root rights as qt.qpa.xcb could not connect to display when superuser
##        see when detailed error when running  :  QT_DEBUG_PLUGINS=1 gns3-webclient-config
##        (this error sentence has lot of forum thread, I found no solution to workaround - So run gns3-webclient as norma user !!)
##      - install schedule : 
##            a) install apk package py3-qt6 and py3-PyQt5
##            b) create gns3-webclient dedicated python venv and activate
##            c) in venv install gns3-webclient-pack module
##            d) run gns3-webclient-config as nomal user
##               note : gns3-webclient-config works, but gns3-webclient-config --install-mime-types doesn't work, that another issue !!
RUN apk --no-cache add \
     py3-qt6  \
     py3-qt5 && \
     mkdir -p /opt/gns3-webclient && \
     cd /opt/gns3-webclient && \
     python3 -m venv --system-site-packages /opt/gns3-webclient/ && \
     cd /opt/gns3-webclient/bin && source activate && \
     python3 -m pip install gns3-webclient-pack  && \
     deactivate
## to set webclient available for each users (aka : abcd and wxyz) don't forget :
##                     - to create /home/<user>/.local/share/mime/packages directly
##                     - and run (as user, no root rgihts !!)  "gns3-webclient-config --install-mime type"     
##   make it persisent forwarding .local/share/mime dir content on kairos overlay dir ??


#************  last  : config and profile personnalization, and container image preloaded from local overlay files
#COPY abcd4objectifipv6 files to /
COPY overlay/files/ / 
# set owners for /home subdirs
RUN chown -R 65535:65535 /home/kairos && chown -R 1000:100 /home/abcd && chown -R 1001:900 /home/wxyz
COPY overlay/files-alpine/ /
COPY overlay/files-iso /
    
