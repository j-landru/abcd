# abcd4**objectifipv6** abcd for **Objectif IPv6** MOOC Labs 

## Objectif IPv6 MOOC Labs
The MOOC (Massive Open On line Course), named "[Objectif IPv6](https://www.fun-mooc.fr/fr/cours/objectif-ipv6/)" on french public [FUN (France Universite Numerique) portal](https://www.fun-mooc.fr) furnish an all in one virtual appliance for its network labs. Objectif IPv6 labs are a set GNS3 (Graphical Network Simulator 3) immutable virtual network models played during the mooc session. abcd4objectifipv6 is the abcd4desktop descendant for that next version MOOC appliance.

## nested virtualization context
GNS3 models run simulated network node (switch, router, host, server, ...), usually in fully virtualized VM (kvm hypervisor) or inside containers. This virtualization GNS3 execution context (second virtualization level) is run inside the appliance itself virtualized (first virtualization level). This version of the appliance leave vyos kvm VM router image, used previously, to switch to docker one. So this abcd instance needs to embed GNS3 simulator and a docker execution context. 

nested virtualisation is :
- Docker node inside HVM on abcd**ik**4objectifipv6 appliance (rootful gns3 docker nodes confined inside HVM VM)
- Docker inside rootless container (DinD or [sysbox](https://github.com/nestybox/sysbox) on abcd**ic**4objectifipv6 appliance

## 20231213 GNS3 now in its own container image, see `../../4mowgli` >>>

- GL(OD)2 : **G**ns3 **L**ab web service **O**n **D**emand **O**n **D**esktop : abcdic4mowgli image embedded inside abcd4objectifipv6 desktop appliance for local lab session on abcdic4objectifip6 desktop container or abcdik4objectifipv6 desktop liveiso baremetal/vm.

So for this desktop GNS3 used case we must embed abcd4mowgli image inside abcd4objectifipv6 image using the same way used to embed alpine image inside abcd4mowgli.

### pull abcd4mowgli inside a rootful abcdic4objectifipv6 running instance with local docker image repository volumed on the overlay dir
then start en ephemeral abcdic4objectifipv6 container as a rootfull container instance with host networking and with volume `-v /tmp/img-store:/tmp/img-store` and `-v <path-abcd4objectifipv6-overlay>/files/var/lib/docker:/var/lib/docker:rw` to abcd4mowgli image inside local repository. Goal is to populate `/var/lib/dokcer `of the overlaydir, which will be copied inside abcd4objectifipv6 image at  build time.

Launch abcdic4objectifipv6 with volumes pointing to the rights host dirs
```
# docker run --privileged --cgroupns=private --ulimit nofile=1024:1024 --network=host --hostname abcdic4objectifipv6 --name abcdic4objectifipv6 --rm -ti \
  -v /rootfs-store/abcd/4desktop/4objectifipv6/overlay/files/var/lib/docker:/var/lib/docker:rw \
  -v /tmp/img-store:/tmp/img-store \
  jlandru/abcdic4objectifipv6:latest
```
Go inside that container
```
# docker exec -ti abcdic4objectifipv6 /bin/bash
```

Import and tag the images

```
abcdic4objectifipv6:/# # need network access to pull docker abcd4mowgli image
abcdic4objectifipv6:/# # verify network config aligned on host
abcdic4objectifipv6:/# ifconfig
abcdic4objectifipv6:/# adjust dns resolver config to host one
abcdic4objectifipv6:/# nano -w /etc/resolv.config
abcdic4objectifipv6:/# # list initial state of docker local repository
abcdic4objectifipv6:/# docker images
abcdic4objectifipv6:/# docker pull <your-docker-registry>abcd4mowgli:latest
abcdic4objectifipv6:/# # tag to abcd4mowgli:aaaammjj
abcdic4objectifipv6:/# docker tag  abcd4mogli:latest abcd4mowgli:20231213
abcdic4objectifipv6:/# # list your local repository content before exiting
abcdic4objectifipv6:/# docker images
REPOSITORY   TAG                        IMAGE ID       CREATED              SIZE
vyos         1.5-rolling-202312080024   238d00f66a61   About a minute ago   1.47GB  xxxxxxxxxxxxxxxxxxxxxxxxx
vyos         latest                     238d00f66a61   About a minute ago   1.47GB  xxxxxxxxxxxxxxxxxxxxxxxxx
alpine       20231208                   f8c20f8bbcb6   14 hours ago         7.38MB  xxxxxxxxxxxxxxxxxxxxxxxxx
alpine       latest                     f8c20f8bbcb6   14 hours ago         7.38MB  xxxxxxxxxxxxxxxxxxxxxxxxx
abcdic4objectifipv6:/# exit
exit
```
Stop abcdic4objectifipv6 container and verify ovelrlay dir content

```
# du -h /rootfs-store/abcd/4desktop/4objectifipv6/overlay/files/var/lib/docker/
```
You're now ready to rebuild abcd4objectifipv6, abcdic4objectifipv6 and abcidk4objectifipv6 images with abcd4mowgli builtin images

