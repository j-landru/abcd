# abcd4**aawwoo** : abcd for  **A**lternative **A**ppliance **W**hen **W**indows is **O**ut of **O**rder. 

## idea ?! 

Since Linux doesn't have the same weaknesses as Windows, it isn't subject to the latter's denial of service. So furnish a SDDM/KDE/Plasma desktop disguised windows11 to help in case of emergency such disaster recovery when under crypto locking attack. Running installless immutable OS to allow partial "corprorate" workload capabilities (web browsing - web mail - Libreoffice - ...)
Why KDE/Plasma disguised windows 11 ? It's more familiar to Windows users, and there's no need to learn how to use a Linux desktop, using almost the same user inteface experience can help relieve user stress in the event of usual workstation deny of service.

## howto
- Start crypto locked baremetal host in live mode without its damaged disk ;
- or remote VDI appliance started on a k8s rescue cluster ;
- or both : run a light minimal baremetal browser in live mode on damaged host to access remote VDI aawwoo hosted on a k8S rescue cluster ?!


### Questions :
- desktop disguished win11, with or without MS Edge browser package ? available on Alpine ?
- local user account or kerberized authentication on corporate AD kdc with pam_kerberos ?
- restore clean backuped user data on home dir ?
- home dir localization ? :
  -- local on COS_PERSITENT labeled encrypted usb key ?
  -- or end to end encrytped remote sshfs mounted using ssh key ?


### Resources : 
#### Plasma win11 theme : see
- some Win11 themes for plasma desktop
             [https://github.com/yeyushengfan258](https://github.com/yeyushengfan258)
             [https://github.com/yeyushengfan258/Win11OS-kde](https://github.com/yeyushengfan258/Win11OS-kde)
             [https://github.com/yeyushengfan258/Win11-icon-theme](https://github.com/yeyushengfan258/Win11-icon-theme)
- Linuxfx  :  Linux distro disguised W11 (kde-plasma or cinnamon)
             [https://www.linuxfx.org/](https://www.linuxfx.org/)
             [https://www.windowsfx.org/index.php](https://www.windowsfx.org/index.php])
