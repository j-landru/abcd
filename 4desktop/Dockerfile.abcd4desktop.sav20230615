
#ARG LUET_VERSION=0.20.6
#FROM quay.io/luet/base:$LUET_VERSION AS luet

# abcd : Alpine Based Cos (formally c3os latter kairos) Derivative
# Alpine derivative from (formally Cos/C3os/Elemental-tollkit, but now Kairos) immutable container image OS
#
# abcd4desktop : a Plamsa/KDE desktop derivative of abcd
#

# this core docker file is inspired by Kairos Alpine example from https://kairos.io/docs/reference/build/
# creation : 20230126 J. Landru : initial kairos example copy
# modification : see comment below


#ARG BASE_IMAGE=jlandru/abcd
# 202303015 try native kairos core-alpine-ubuntu image form quay.io
#ARG BASE_IMAGE=quay.io/kairos/core-alpine-ubuntu
# 20230320 trying roolback on abcd
ARG BASE_IMAGE=jlandru/abcd:latest

FROM $BASE_IMAGE

#  desktop packages for abcd
RUN apk --no-cache add \
#      plasma-desktop \
#      systemsettings \
#      sddm \
#      breeze \
#      elogind \
#      polkit \
#      polkit-elogind \
#      dbus \
      plasma \
      sddm \
      sddm-openrc \
      sddm-kcm \
#      20230301 unable to make wayland working ??
#               and remote display with xpra works only with X11 not wayland
#      wayland \
#      xwayland \
      utmps \
      utmps-openrc \
      elogind \
      polkit \
      polkit-elogind \
      dbus \
      musl-locales \
      openrc-settingsd \
      pciutils \
      xterm \
      linux-firmware \
      xf86-video-vmware \
      xf86-input-wacom \
      xf86-input-vmmouse \
      xf86-video-amdgpu \
      xf86-video-nv \
      xf86-video-ati \
      xf86-video-intel \
      xf86-video-nouveau \
      xf86-input-synaptics \
      xf86-video-qxl \
      xf86-input-mtrack \
      xf86-video-dummy \
      xf86-video-modesetting \
#      xf86-video-geode \
      xf86-input-libinput \
      xf86-input-evdev \
      xf86-video-apm \
      xf86-video-ark \
      xf86-video-ast \
      xf86-video-chips \
      xf86-video-fbdev \
      xf86-video-i128 \
      xf86-video-i740 \
      xf86-video-openchrome \
      xf86-video-r128 \
      xf86-video-rendition \
      xf86-video-s3virge \
      xf86-video-savage \
      xf86-video-siliconmotion \
      xf86-video-sis \
      xf86-video-tdfx \
      xf86-video-vesa \
      xf86-video-ati \
#      xf86-video-vboxvideo \
      libdrm \
      drm_info \
      mesa-dri-gallium \
      mesa-va-gallium \
      libva-intel-driver \
      qemu-hw-display-virtio-gpu-gl \
      qemu-hw-display-virtio-vga \
      qemu-hw-display-virtio-vga-gl \
      qemu-hw-display-virtio-gpu \
      qemu-hw-display-virtio-gpu-pci-gl \
      qemu-hw-display-virtio-gpu-pci \
      qemu-virtiofsd \
      virtualbox-guest-additions \
      virtualbox-guest-additions-x11 \
      hvtools \
      kde-applications-admin \
      kde-applications-base \
      kde-applications-network \
      kde-applications-utils \
# 20221219 adding xpra to play plasma/kde desktop when running inside container
#          clientless (browser only) remote desktop over https 
#      xpra \ ## 20230320 test with latest version 4.4.4-r1 from edge branch
#      xpra-test \
##      xpra-webclient \ ## waiting for ver >= 7.0 avalaibility, see manual install below
##      xpra-openrc \
##      py3-cairo \
##      py3-cairosvg \
##      py3-paramiko \
##      py3-uinput \
##      py3-netifaces \
##      py3-pillow \
##      py3-rencode \
##      py3-xdg \
##      py3-opengl \
##      py3-gst \
##      # 20230313 lbixcb, xpra needs xtest
##      libxcb \
##      libxinerama \
##      xinput \
##      xev \
## #      xrandr \
##      xdpyinfo \
##      xrdb \
##      xwininfo \
##      xvinfo \
##      xdriinfo \
##      xprop \
##      pulseaudio-utils \
##      git \
# 20230207 current desktop packages
      firefox-esr \
#xxx 202305025 xpra now on latest version on Alpine
      xrandr \
      xpra \
      xpra-pyc \
      xpra-openrc \
      xpra-webclient
      
#20230530
RUN apk --no-cache add -X http://dl-cdn.alpinelinux.org/alpine/edge/testing \
      py3-pam-pyc

#xxx 202305025 xpra now on latest version on Alpine
#xxx# 20230320 trying to get dynamic X resolution in xpra with more latest xrandr version !!
#xxxRUN apk --no-cache add -X http://dl-cdn.alpinelinux.org/alpine/edge/community \
#xxx      xrandr \
#xxx      xpra=4.4.4-r2 \
#xxx      xpra-openrc=4.4.4-r2 \
#xxx      #xpra-webclient=6.2-r0 \
#xxx      tiff \
#xxx      ##py3-cairo \
#xxx      ##py3-cairosvg \
#xxx      ##py3-paramiko \
#xxx      ##py3-uinput \
#xxx      ##py3-netifaces \
#xxx      ##py3-pillow \
#xxx      ##py3-rencode \
#xxx      ##py3-xdg \
#xxx      ##py3-opengl \
#xxx      #py3-gst \
#xxx      ## # 20230313 lbixcb, xpra needs xtest
#xxx      ##libxcb \
#xxx      ##libxinerama \
#xxx      ##xinput \
#xxx      ##xev \
#xxx      ##xdpyinfo \
#xxx      ##xrdb \
#xxx      ##xwininfo \
#xxx      ##xvinfo \
#xxx      ##xdriinfo \
#xxx      ##xprop \
#xxx      ##pulseaudio-utils \
#xxx      git 
#xxx# 20230314 trying xpra-html5 7.0 : manual install waiting Alpine package >= 7.0
#xxxRUN cd /tmp && \
#xxx      git clone https://github.com/Xpra-org/xpra-html5 && \
#xxx      cd xpra-html5 && \
#xxx      ./setup.py install && \
#xxx      cd .. && rm -rf xpra-html5


# 20230517 Network management, switch from connman to networkmanager
#          the latter is natively and better integrated inside KDE/Plasma 
RUN apk --no-cache add \
      networkmanager \
      networkmanager-openrc \
      networkmanager-tui \
      networkmanager-cli  
# switch to networkmanager at boot
#RUN rc-update del connman boot && rc-update add networkmanager boot
#RUN rc-update del connman boot && rc-update add networkmanager default
# 20230525  OK but not sufficient for abcdik as connman configured automatically on alpine kairos ISO
# see https://kairos.io/docs/advanced/networking/
# so for abcdik probably need apply again "rc-update del connman boot && rc-update add networkmanager boot" at bootup !!
#
# allow all user to configure network using NetworkManager
# see https://wiki.alpinelinux.org/wiki/NetworkManager#not_authorized_to_control_networking
RUN mkdir -p /etc/NetworkManager/conf.d && cat >/etc/NetworkManager/conf.d/any-user.conf <<EOF
[main]
auth-polkit=false

EOF

# 20230613 try to speed up openrc which is so long especially under docker at abcdic startup...
#          set also rc_logger to yes 
RUN  cp /etc/rc.conf /etc/rc.conf.orig && \
     sed -i '/#rc_parallel="NO"/c\#rc_parallel="NO"\nrc_parallel="YES"' /etc/rc.conf && \
     sed -i '/#rc_logger="NO"/c\#rc_logger="NO"\nrc_logger="YES"' /etc/rc.conf

# 20230517 remove wayland sessions from sddm, only X11 sessions.
#          why ?
#          1) wayland not yet remote desktop friendly as X11
#             need same desktop user xperience on abcdik (bare metal GPU or HVM spice/qxl)
#             as well on abcdic (inside container) whrere desktop is remote through xpra (X11 only)
#          2) unable to find a wayland working config for KDE/Plasma on Alpine, need more investigation...
RUN rm /usr/share/wayland-sessions/*


# abcd4desktop openrc init (dbus, elogind,utmpd, polkit) needed for X / sddm / plamsa / KDE
##RUN      ln -sf /etc/init.d/dbus /etc/runlevels/default/dbus  && \
## try dbus at boot runlevel as long start in docker and some other service depend dbus
RUN      ln -sf /etc/init.d/dbus /etc/runlevels/boot/dbus  && \
      ln -sf /etc/init.d/elogind /etc/runlevels/boot/elogind-cgroup  && \
      ln -sf /etc/init.d/elogind /etc/runlevels/boot/elogind  && \
      ln -sf /etc/init.d/elogind /etc/runlevels/boot/utmpd  && \
      ln -sf /etc/init.d/polkit /etc/runlevels/default/polkit

# 20230505
#   About KDE kscreenlocker unable to unlock screen even with the right passwd !!!
#   found here : https://forum.kde.org/viewtopic.php?t=152045
#    on Alpine, rights change needed on /sbin/unix_chkpwd  using this command
RUN chmod u+s /sbin/unix_chkpwd


#COPY files/ /
COPY overlay/files/ /
COPY overlay/files-alpine/ /
copy overlay/files-iso /
